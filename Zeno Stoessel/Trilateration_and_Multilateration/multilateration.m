clc
clear
close all

% ref points coordinates 
A = [0      0       3570];
B = [0      4790    3590];
C = [8390   4210    3590];
D = [8180   1890    2000];
E = [3610   680     4000];

%raw data
d_A = [6.424 6.412 6.426 6.427 6.423 6.422 6.425 6.430 6.421 6.430 6.431 6.439 6.434 6.418 6.445 6.421 6.425 6.419 6.418 6.421 6.418 6.424 6.430 6.423 6.423 6.423 6.424 6.434 6.439 6.433 6.429 6.429 6.438 6.423 6.437 6.423 6.444 6.444 6.441 6.430]*1000;
d_B = [5.911 5.920 5.921 5.919 5.920 5.925 5.921 5.921 5.925 5.930 5.920 5.924 5.920 5.915 5.922 5.917 5.923 5.918 5.913 5.924 5.918 5.919 5.921 5.921 5.916 5.929 5.924 5.915 5.925 5.920 5.918 5.921 5.921 5.915 5.918 5.921 5.918 5.920 5.922 5.924]*1000;
d_C = [4.389 4.384 4.380 4.383 4.378 4.384 4.390 4.377 4.389 4.369 4.382 4.375 4.391 4.382 4.381 4.382 4.371 4.372 4.380 4.377 4.379 4.373 4.375 4.376 4.375 4.378 4.371 4.378 4.374 4.381 4.374 4.381 4.377 4.377 4.371 4.383 4.380 4.378 4.375 4.377]*1000;
d_D = [3.605 3.608 3.606 3.604 3.612 3.614 3.608 3.608 3.608 3.607 3.607 3.605 3.602 3.614 3.610 3.610 3.608 3.610 3.610 3.609 3.608 3.608 3.608 3.610 3.611 3.612 3.609 3.609 3.608 3.613 3.614 3.611 3.612 3.613 3.614 3.610 3.609 3.609 3.608 3.609]*1000;
d_E = [3.734 3.743 3.744 3.747 3.749 3.741 3.739 3.744 3.739 3.743 3.739 3.745 3.746 3.747 3.735 3.739 3.745 3.749 3.740 3.745 3.737 3.730 3.745 3.736 3.749 3.746 3.747 3.741 3.744 3.741 3.740 3.743 3.740 3.741 3.746 3.739 3.750 3.739 3.741 3.751]*1000;

% create a single matrix for easy acces
d = [d_A; d_B; d_C; d_D; d_E];
ref = [A; B; C; D; E];

% create distance matrix according to http://edge.cs.drexel.edu/regli/Classes/CS680/Papers/Localization/trildbl-2.pdf
E_ = zeros(3,1);
for m = 1:3
    for n =1:3
    E_(m,n) = ref(m+1,n)-ref(1,n);
    end
end
b = zeros(3,length(d_A));
for n = 1:3
b(n,:) = 1/2*(d(1,:).^2-d(n+1,:).^2+ (ref(n+1,1)-ref(1,1))^2+ (ref(n+1,2)-ref(1,2))^2 ...
    +(ref(n+1,3)-ref(1,3))^2);
end
b;

%calculate coordinate
coordinate = inv(E_)*b+ref(1,:)';
coordinate2 = (E_'*E_)^-1*E_'*b+ref(1,:)';

figure('Name', 'Scaled Room')
plot3(coordinate(1,:),coordinate(2,:),coordinate(3,:),'x')
hold on
plot3(ref(:,1),ref(:,2),ref(:,3),'xr')
axis vis3d equal
xlabel('x axis [mm]');
ylabel('y axis [mm]');
zlabel('z axis [mm]');
legend('calculated coordinates','ref points');
grid on

axis([0 8390 0 4790 0 4000])

ax = 0;
ay = 0;
az = 3570;
a_txt = 'A';
text(ax,ay,az- 300,a_txt,'HorizontalAlignment','right')

bx = 0;
by = 4790;
bz = 3590;
b_txt = 'B';
text(bx+500,by,bz-300,b_txt,'HorizontalAlignment','right')

cx = 8390;
cy = 4210;
cz = 3590;
c_txt = 'C';
text(cx+500,cy,cz-300,c_txt,'HorizontalAlignment','right')

dx = 8180;
dy = 1890;
dz = 2000;
d_txt = 'D';
text(dx+500,dy,dz-300,d_txt,'HorizontalAlignment','right')

ex = 3610;
ey = 680;
ez = 4000;
e_txt = 'E';
text(ex+500,ey,ez-300,e_txt,'HorizontalAlignment','right')



figure(2)
q_1 = quantile(coordinate(1,:),0.95);
subplot(131)
boxplot(coordinate(1,:));
q_2 = quantile(coordinate(2,:),0.95);
xlabel('x axis');
ylabel('coordinate [mm]');
subplot(132)
boxplot(coordinate(2,:));
q_3 = quantile(coordinate(3,:),0.95);
xlabel('y axis');
ylabel('coordinate [mm]');
subplot(133)
boxplot(coordinate(3,:));
xlabel('z axis');
ylabel('coordinate [mm]');


%% Least Squares Algorithm
[N1 N2] = Multilat_LQ(ref',d',0);