clc
clear
close all

N = 8

A = [6.424 6.412 6.426 6.427 6.423 6.422 6.425 6.430 6.421 6.430 6.431 6.439 6.434 6.418 6.445 6.421 6.425 6.419 6.418 6.421 6.418 6.424 6.430 6.423 6.423 6.423 6.424 6.434 6.439 6.433 6.429 6.429 6.438 6.423 6.437 6.423 6.444 6.444 6.441 6.430]*1000;
B = [5.911 5.920 5.921 5.919 5.920 5.925 5.921 5.921 5.925 5.930 5.920 5.924 5.920 5.915 5.922 5.917 5.923 5.918 5.913 5.924 5.918 5.919 5.921 5.921 5.916 5.929 5.924 5.915 5.925 5.920 5.918 5.921 5.921 5.915 5.918 5.921 5.918 5.920 5.922 5.924]*1000;
C = [4.389 4.384 4.380 4.383 4.378 4.384 4.390 4.377 4.389 4.369 4.382 4.375 4.391 4.382 4.381 4.382 4.371 4.372 4.380 4.377 4.379 4.373 4.375 4.376 4.375 4.378 4.371 4.378 4.374 4.381 4.374 4.381 4.377 4.377 4.371 4.383 4.380 4.378 4.375 4.377]*1000;
D = [3.605 3.608 3.606 3.604 3.612 3.614 3.608 3.608 3.608 3.607 3.607 3.605 3.602 3.614 3.610 3.610 3.608 3.610 3.610 3.609 3.608 3.608 3.608 3.610 3.611 3.612 3.609 3.609 3.608 3.613 3.614 3.611 3.612 3.613 3.614 3.610 3.609 3.609 3.608 3.609]*1000;
E = [3.734 3.743 3.744 3.747 3.749 3.741 3.739 3.744 3.739 3.743 3.739 3.745 3.746 3.747 3.735 3.739 3.745 3.749 3.740 3.745 3.737 3.730 3.745 3.736 3.749 3.746 3.747 3.741 3.744 3.741 3.740 3.743 3.740 3.741 3.746 3.739 3.750 3.739 3.741 3.751]*1000;

%messung nach verschiebung Referenztisch
refC = 4.359;
refE = 5.907;
refA = 6.411;
refD = 3.594;

leng = length(A)

figure(1)

histogram(A,N)
title('Reference point A')
xlabel('Distance [mm]')
ylabel('Number of measurements [#]')
minA=min(A)
mxA=max(A)
mnA=mean(A)
mdA=median(A)
stdvA=std(A)

% Create the labels
minlabel=sprintf('Min -- %.0f mm', minA);
maxlabel=sprintf('Max -- %.0f mm', mxA);
mnlabel=sprintf('Mean   --  %.1f mm', mnA);
mdlabel=sprintf('Median --  %.1f mm', mdA);
stdlabel=sprintf('Std Deviation -- %.2f mm', stdvA);
% Create the textbox
h=annotation('textbox',[0.15 0.8 0.1 0.1]);
set(h,'String',{minlabel, maxlabel,mnlabel, mdlabel, stdlabel});


figure(2)

histogram(B,N)
title('Reference point B')
xlabel('Distance [mm]')
ylabel('Number of measurements [#]')
minB=min(B)
mxB=max(B)
mnB=mean(B)
mdB=median(B)
stdvB=std(B)

% Create the labels
minlabel=sprintf('Min -- %.0f mm', minB);
maxlabel=sprintf('Max -- %.0f mm', mxB);
mnlabel=sprintf('Mean   --  %.1f mm', mnB);
mdlabel=sprintf('Median --  %.1f mm', mdB);
stdlabel=sprintf('Std Deviation -- %.2f mm', stdvB);
% Create the textbox
h=annotation('textbox',[0.15 0.8 0.1 0.1]);
set(h,'String',{minlabel, maxlabel,mnlabel, mdlabel, stdlabel});


figure(3)

histogram(C,N)
title('Reference point C')
xlabel('Distance [mm]')
ylabel('Number of measurements [#]')
minC=min(C)
mxC=max(C)
mnC=mean(C)
mdC=median(C)
stdvC=std(C)

% Create the labels
minlabel=sprintf('Min -- %.0f mm', minC);
maxlabel=sprintf('Max -- %.0f mm', mxC);
mnlabel=sprintf('Mean   --  %.1f mm', mnC);
mdlabel=sprintf('Median --  %.1f mm', mdC);
stdlabel=sprintf('Std Deviation -- %.2f mm', stdvC);
% Create the textbox
h=annotation('textbox',[0.15 0.8 0.1 0.1]);
set(h,'String',{minlabel, maxlabel,mnlabel, mdlabel, stdlabel});


figure(4)

histogram(D,N)
title('Reference point D')
xlabel('Distance [mm]')
ylabel('Number of measurements [#]')
minD=min(D)
mxD=max(D)
mnD=mean(D)
mdD=median(D)
stdvD=std(D)

% Create the labels
minlabel=sprintf('Min -- %.0f mm', minD);
maxlabel=sprintf('Max -- %.0f mm', mxD);
mnlabel=sprintf('Mean   --  %.1f mm', mnD);
mdlabel=sprintf('Median --  %.1f mm', mdD);
stdlabel=sprintf('Std Deviation -- %.2f mm', stdvD);
% Create the textbox
h=annotation('textbox',[0.15 0.8 0.1 0.1]);
set(h,'String',{minlabel, maxlabel,mnlabel, mdlabel, stdlabel});



figure(5)

histogram(E,N)
title('Reference point E')
xlabel('Distance [mm]')
ylabel('Number of measurements [#]')
minE=min(E)
mxE=max(E)
mnE=mean(E)
mdE=median(E)
stdvE=std(E)

% Create the labels
minlabel=sprintf('Min -- %.0f mm', minE);
maxlabel=sprintf('Max -- %.0f mm', mxE);
mnlabel=sprintf('Mean   --  %.1f mm', mnE);
mdlabel=sprintf('Median --  %.1f mm', mdE);
stdlabel=sprintf('Std Deviation -- %.2f mm', stdvE);
% Create the textbox
h=annotation('textbox',[0.15 0.8 0.1 0.1]);
set(h,'String',{minlabel, maxlabel,mnlabel, mdlabel, stdlabel});

figure(6)

subplot(221)


c = categorical({'A','B','C','D','E'});
minmax = [minA mxA; minB mxB; minC mxC; minD mxD; minE mxE];
b = bar(c,minmax,'b')
b(1).FaceColor = 'r'
ylim([3500 6500])
legend('Minimal distance','Maximum distance')
title('Minimum & Maximum')
xlabel('Points')
ylabel('Distance [mm]')

subplot(222)
c = categorical({'A','B','C','D','E'});
meanvalues = [mnA mnB mnC mnD mnE];
b = bar(c,meanvalues,'b')


subplot(223)
boxplot(A)
subplot(224)


figure(7)
subplot(231)
boxplot(A,'A');
ylabel('Distance [mm]');
subplot(232)
boxplot(B,'B');
ylabel('Distance [mm]');
subplot(233)
boxplot(C,'C');
ylabel('Distance [mm]');
subplot(234)
boxplot(D,'D')
ylabel('Distance [mm]')
subplot(235)
boxplot(E,'E')
ylabel('Distance [mm]')