clc
clear
close all

%% calculate coordinate with trilateration
% ref points coordinates A,B,C
A = [0;      0;       3570];
B = [0;      4790;    3590];
C = [8390;   4210;    3590];

e_x = (B-A)./norm(B-A);
d = norm(B-A);
i = e_x.*(C-A);
e_y = (C-A-norm(i).*e_x)./norm(C-A-norm(i).*e_x);
j = e_y.*(C-A);

r1 = [6.424 6.412 6.426 6.427 6.423 6.422 6.425 6.430 6.421 6.430 6.431 6.439 6.434 6.418 6.445 6.421 6.425 6.419 6.418 6.421 6.418 6.424 6.430 6.423 6.423 6.423 6.424 6.434 6.439 6.433 6.429 6.429 6.438 6.423 6.437 6.423 6.444 6.444 6.441 6.430]*1000;
r2 = [5.911 5.920 5.921 5.919 5.920 5.925 5.921 5.921 5.925 5.930 5.920 5.924 5.920 5.915 5.922 5.917 5.923 5.918 5.913 5.924 5.918 5.919 5.921 5.921 5.916 5.929 5.924 5.915 5.925 5.920 5.918 5.921 5.921 5.915 5.918 5.921 5.918 5.920 5.922 5.924]*1000;
r3 = [4.389 4.384 4.380 4.383 4.378 4.384 4.390 4.377 4.389 4.369 4.382 4.375 4.391 4.382 4.381 4.382 4.371 4.372 4.380 4.377 4.379 4.373 4.375 4.376 4.375 4.378 4.371 4.378 4.374 4.381 4.374 4.381 4.377 4.377 4.371 4.383 4.380 4.378 4.375 4.377]*1000;

x = (r1.^2-r2.^2+d^2)/(2*d);
y = (r1.^2-r3.^2+norm(i)^2+norm(j)^2)/(2*norm(j))-norm(i)/norm(j).*x;
z1 = sqrt(r1.^2-x.^2-y.^2);

coordinate = (A+x.*e_x+y.*e_y-z1.*cross(e_y,e_x));

figure('Name', 'Point in Room')
plot3(coordinate(1,:),coordinate(2,:),coordinate(3,:),'.')
hold on
plot3([A(1) B(1) C(1)],[A(2) B(2) C(2)],[A(3) B(3) C(3)],'xr')
axis vis3d equal
xlabel('x axis [mm]');
ylabel('y axis [mm]');
zlabel('z axis [mm]');
legend('calculated coordinates','ref points');
grid on
axis([0 8400 0 4800 0 4100])

figure('Name', 'Calculated x, y, z Coordinates with Ref. A,B,C')
q_1 = quantile(coordinate(1,:),0.95);
subplot(131)
boxplot(coordinate(1,:));
q_2 = quantile(coordinate(2,:),0.95);
xlabel('x axis');
ylabel('coordinate [mm]');
subplot(132)
boxplot(coordinate(2,:));
q_3 = quantile(coordinate(3,:),0.95);
xlabel('y axis');
ylabel('coordinate [mm]');
subplot(133)
boxplot(coordinate(3,:));
xlabel('z axis');
ylabel('coordinate [mm]');

%% calculate coordinate with trilateration
% ref points coordinates C,D,E

C = [8390;   4210;    3590];
D = [8180;   1890;    2000];
E = [3610;   680;     4000];

e_x = (D-C)./norm(D-C);
d = norm(D-C);
i = e_x.*(E-C);
e_y = (E-C-norm(i).*e_x)./norm(E-C-norm(i).*e_x);
j = e_y.*(E-C);

x = (r1.^2-r2.^2+d^2)/(2*d);
y = (r1.^2-r3.^2+norm(i)^2+norm(j)^2)/(2*norm(j))-norm(i)/norm(j).*x;
z1 = sqrt(r1.^2-x.^2-y.^2);

coordinate = (C+x.*e_x+y.*e_y-z1.*cross(e_y,e_x));

figure('Name', 'Point in Room')
plot3(coordinate(1,:),coordinate(2,:),coordinate(3,:),'.')
hold on
plot3([C(1) D(1) E(1)],[C(2) D(2) E(2)],[C(3) D(3) E(3)],'xr')
axis vis3d equal
xlabel('x axis [mm]');
ylabel('y axis [mm]');
zlabel('z axis [mm]');
legend('calculated coordinates','ref points');
grid on
axis([0 8400 0 4800 0 4100])

figure('Name', 'Calculated x, y, z Coordinates with Ref. C,D,E')
q_1 = quantile(coordinate(1,:),0.95);
subplot(131)
boxplot(coordinate(1,:));
q_2 = quantile(coordinate(2,:),0.95);
xlabel('x axis');
ylabel('coordinate [mm]');
subplot(132)
boxplot(coordinate(2,:));
q_3 = quantile(coordinate(3,:),0.95);
xlabel('y axis');
ylabel('coordinate [mm]');
subplot(133)
boxplot(coordinate(3,:));
xlabel('z axis');
ylabel('coordinate [mm]');


%% demo of measurement error
% given an accurate distance r1 and an inaccurate distance r2 and r3 with
% uniform random distribution

error = 100*rand(1,10000)-50; %measurement error of +- 100mm
r3_err = r3(1)+error;
r1_ref = r1(1)+100*rand(1,10000)-50;
r2_err = r2(1)+100*rand(1,10000)-50;

x = (r1_ref.^2-r2_err.^2+d^2)/(2*d);
y = (r1_ref.^2-r3_err.^2+norm(i)^2+norm(j)^2)/(2*norm(j))-norm(i)/norm(j).*x;
z1 = -sqrt(r1_ref.^2-x.^2-y.^2);

coordinate = (A+x.*e_x+y.*e_y+z1.*cross(e_y,e_x));

figure('Name', 'Calculated Coordinates With Error');
%plot3(coordinate(1,:),coordinate(2,:),coordinate(3,:),'.')
dist_x = 4800:5100; 
dist_y = 2900:3200;
[xq, yq, zq] = meshgrid(dist_x,dist_y,0);
v = 1;
vq = griddata(coordinate(1,:),coordinate(2,:),coordinate(3,:),xq,yq,'cubic');
mesh(xq,yq,vq)
axis vis3d 
xlabel('x axis [mm]');
ylabel('y axis [mm]');
zlabel('z axis [mm]');
grid on
legend('calculated coordinates');
%%
% tolerances of the reference points
% 

A = [0;      0;       3570];
B = [0;      4790;    3590];
C = [8390;   4210;    3590];

A = A + 100*rand(1,10000)-50;
B = B+100*rand(1,10000)-50;
C = C*ones(1,length(A));

r1_ref = r1(1);
r2_ref = r2(1);
r3_ref = r3(1);



x = zeros(1,length(B));
y = zeros(1,length(B));
z1= zeros(1,length(B));
for n = 1:length(B)
    
e_x = (B(:,n)-A(:,n))./norm(B(:,n)-A(:,n));
d = norm(B(:,n)-A(:,n));
i = e_x.*(C(:,n)-A(:,n));
e_y = (C(:,n)-A(:,n)-norm(i).*e_x)./norm(C(:,n)-A(:,n)-norm(i).*e_x);
j = e_y.*(C(:,n)-A(:,n));

x(n) = (r1_ref.^2-r2_ref.^2+d^2)/(2*d);
y(n) = (r1_ref.^2-r3_ref.^2+norm(i)^2+norm(j)^2)/(2*norm(j))-norm(i)/norm(j).*x(n);
z1(n) = -sqrt(r1_ref.^2-x(n).^2-y(n).^2);
end

coordinate = (A+x.*e_x+y.*e_y-z1.*cross(e_y,e_x));

figure('Name', 'Calculated Coordinates With Error');
dist_x = 4800:5100; 
dist_y = 2900:3200;
[xq, yq, zq] = meshgrid(dist_x,dist_y,0);
v = 1;
vq = griddata(coordinate(1,:),coordinate(2,:),coordinate(3,:),xq,yq,'cubic');
mesh(xq,yq,vq)
axis vis3d 
xlabel('x axis [mm]');
ylabel('y axis [mm]');
zlabel('z axis [mm]');
grid on
legend('calculated coordinates');

