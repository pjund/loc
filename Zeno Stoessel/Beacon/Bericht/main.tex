
\documentclass[journal]{IEEEtran}
\usepackage{blindtext}
\usepackage{graphicx}
\usepackage[framed,numbered,autolinebreaks,useliterate]{mcode}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{setspace}


\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Laboratory Short Course: RSS}


\author{Mario~Fischer,~Pascal~Jund}




% The paper headers
\markboth{LOC 15.3.2017}{}

\maketitle



\section{1. Location}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\textwidth]{Bilder/IMG_0176}
    \caption{Beacon location}
    \label{fig:beacon_loc}
\end{figure}

The Beacon (Nr. 12) was placed directly next to the door of the room D311 as depicted in figure \ref{fig:beacon_loc}. The signal strength was measured all along the corridor. There were no obstacle between the beacon and the mobile phone which could disturb the signal. We have to be aware, that the walls will significantly influence the signal strength due to the reflection of the electromagnetic wave.

\section{2. Software}
To measure the signal strength of the beacon, we used an Apple iPhone 7 with the "Estimote" app.

\newpage

\section{3. Measurements \& 4./5. Data processing}
The figure \ref{fig:boxplot} shows the measured signalstrength in $dBm$ depending to the measured distance in $m$

\begin{figure}[h]
    \centering
    \includegraphics[width=0.55\textwidth,left]{Bilder/boxplot}
    \caption{Boxplot of signal strength depending to the distance}
    \label{fig:boxplot}
\end{figure}

Due to the nonlinear distance scaling, a scaled plot was made, to get a feeling how the signal strength behaves according to the distance. The plot \ref{fig:boxplot_scaled} shows the same boxplot with a real scaled distance.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.55\textwidth,left]{Bilder/boxplot_scaled}
    \caption{Boxplot of signal strength depending to the scaled distance}
    \label{fig:boxplot_scaled}
\end{figure}

\newpage

\section{6. Comparison}
Equation \ref{equ:power} provides an approximation of the signal power. All values are available, except for the power of $n$. $n$ effects the shape of the curve, it has to be chosen in an iterative way. 

\begin{equation} 
P_{received}(d) \approx P_{received}(d_0)*\left(\frac{d_0}{d}\right)^n
\label{equ:power}
\end{equation}

In the example Fig. \ref{fig:power_model} $n$ was determined in an iterative way by two given points ($x$ reference point and $n$ optimisation point). The $reference~point$ can be specified in the Matlab script. The $optimisation~point$ is allways the furthest measured point, in this example at $30m$.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth,center]{Bilder/logarythmic_iterative}
    \caption{Logarythmic model}
    \label{fig:power_model}
\end{figure}

The following Matlab script determines $n$ at a resolution of 0.001. In this example, where the 
$reference~point$ was chosen at 2m, the n which fits the best was calculated at $n=2.0408$


\begin{lstlisting}
ref_dist = 10; %reference distance (the x-th point) 10 => 2m

d = 0:0.1:30;
p_ref = 10.^(median(rss_meas(ref_dist,:))./10)*0.001;

iteration = 0;
n = -1;
while(iteration == 0)
    p_approx = p_ref * (dist(ref_dist)./d).^n;
    n = n+0.0001;
    a = p_approx(length(p_approx));
    b = 10.^(median(rss_meas(length(...
    rss_meas),:))./10)*0.001;
    if(a <= b)
        iteration = 1;
        n
    end       
end

RSS_approx = 10*log10(p_approx./0.001);

\end{lstlisting}





\section{7. Linear regression}

The linear regression is the curve which fits best to a given dataset. This is achieved by the minimization of the squared error of each point in the dataset and the linear function. 

\begin{equation} 
   f(\alpha_0,\alpha_1) = \min_{\alpha_0,\alpha_1} \left\{  \sum_{i=1}^n R(\alpha_0,\alpha_1)^2  \right\}
\end{equation}


\begin{equation}
    R(\alpha_0,\alpha_1) = rss_i - (\alpha_0 + \alpha_1 * d_i)
\end{equation}
\newline
The function $f(\alpha_0,\alpha_1)$ has to be derived by $\alpha_0$ \& $\alpha_1$ and set to zero, to minimize the sum of the squared residuals.

\begin{equation}
   \frac{\mathrm d}{\mathrm d \alpha_0} f(\alpha_0,\alpha_1) = -2 \sum_{i=1}^n rss_i - (\alpha_0 + \alpha_1 * d_i) \overset{!}{=} 0
   \end{equation}
   
\begin{equation}
   \frac{\mathrm d}{\mathrm d \alpha_1} f(\alpha_0,\alpha_1) = -2 \sum_{i=1}^n (rss_i - (\alpha_0 +\alpha_1 * d_i)) *d_i \overset{!}{=} 0
\end{equation}
 \newline
 This leads to an equation system with two unknown variables and two functions.
 
\begin{equation}
  \begin{aligned}
    \sum_{i=1}^n \alpha_0 + \sum_{i=0}^n \alpha_1 * d_i &= \sum_{i=1}^n rss_i \\
    \sum_{i=1}^n \alpha_0 * d_i + \sum_{i=1}^n \alpha_1 * d_i^2 &= \sum_{i=1}^n rss_i * d_i
  \end{aligned}
\end{equation}
\newline
The equation system can easily be resolved by transforming the first equation to $\alpha_0$ and replace $\alpha_0$ in the second equation. The second equation can now be transformed to $\alpha_1$ which leads to the following result:

\begin{equation}
\begin{aligned}
  \alpha_1 &= \frac{\sum_{i=1}^n d_i * rss_i -  \frac{\sum_{i=1}^n rss_i * \sum_{i=1}^n d_i}{n} }{\sum_{i=1}^n d_i^2 - \frac{(\sum_{i=1}^n d_i)^2}{n}} \\
  \alpha_1 &= \frac{\sum_{i=1}^n d_i * rss_i - n * \bar{x}*\bar{y}}{\sum_{i=1}^n d_i^2 - n*\bar{d_i}^2}
  \end{aligned}
\end{equation}

\begin{equation}
  \alpha_0 = \overline{{rss}} - \alpha_1 * \bar{x}
\end{equation}
\newline
The dataset can now be approximated by the function \ref{equ:function}

\begin{equation}
  rss = \alpha_1 * d + \alpha_0
  \label{equ:function}
\end{equation}
\newline
$\alpha_0$ and $\alpha_1$ was calculated with the following Matlab script:

\begin{lstlisting}
rss_mean = zeros(1,length(rss_meas(:,1)));
for i=1:length(rss_meas(:,1))
    rss_mean(i) = mean(rss_meas(i,:));
end

n = length(rss_mean);

alpha1 = (sum(rss_mean.*dist) - n*mean(dist)*mean(rss_mean)) / (sum(dist.^2) - n*mean(dist)^2);
alpha2 = mean(rss_mean) - alpha1 * mean(dist);

x = 0:0.1:30;
y = alpha1.*x + alpha2;

figure(5)
hold on
plot(x,y)
boxplot(rss_meas','position',dist,'labels'...
,dist_string)
xlabel('Distance [m]')
ylabel('RSS [dBm]')
legend('Linear regression')
\end{lstlisting}

\doublespacing
Which leads to the plot in figure \ref{fig:lin_reg}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth,center]{Bilder/linear_regression}
    \caption{Linear regression}
    \label{fig:lin_reg}
\end{figure}


\end{document}












