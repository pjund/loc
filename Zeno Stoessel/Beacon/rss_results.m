%% RSSI Measurement results
clc
clear
close all

rss_meas = -[
48	45	43	50	47	46	50	51	59	47	51	50	49	44	50	51	48	46	51	55;
45	42	42	48	45	44	47	47	44	50	47	45	45	50	50	44	43	50	51	50;
51	56	55	55	51	55	59	58	54	55	55	54	56	60	55	53	50	55	56	53;
46	48	45	62	53	44	49	61	52	49	47	65	46	43	60	44	55	56	44	44;
52	53	49	47	62	48	53	49	52	63	63	62	84	53	60	62	49	60	48	54;
52	49	49	52	48	50	55	48	53	50	52	49	50	52	54	50	53	55	54	49;
52	51	51	54	50	50	52	55	50	50	52	55	50	50	51	55	54	53	57	53;
50	50	60	67	58	63	59	59	59	60	59	59	60	61	59	61	60	60	61	63;
59	53	54	54	54	57	56	53	55	58	55	53	59	56	54	57	58	55	55	61;
58	57	58	58	57	58	58	55	54	55	55	55	58	58	63	69	68	64	63	61;
61	64	58	66	59	63	67	57	61	62	68	55	55	61	58	61	61	69	71	72;
68	66	65	67	64	65	61	62	62	61	62	61	62	65	62	63	65	63	63	63;
72	63	64	69	68	63	69	67	62	61	67	65	65	61	73	68	61	68	66	61;
65	77	66	70	63	66	67	61	64	65	68	66	66	64	63	67	65	59	60	67;
68	73	68	67	67	64	70	65	70	84	74	73	72	69	72	70	71	79	74	68;
78	74	75	76	83	78	75	78	73	74	75	78	80	77	76	76	73	73	77	76;
73	74	75	76	75	81	74	75	78	80	75	74	75	80	77	71	72	76	78	77;
83	90	84	81	80	78	92	82	79	80	79	81	82	83	85	83	83	80	80	81;
80	80	77	87	81	79	80	82	77	76	80	88	85	86	77	85	82	86	81	91;
82	84	82	77	84	88	88	82	80	80	81	80	80	81	80	81	83	88	86	82];

dist = [0.1 0.15 0.2 0.3 0.4 0.5 0.7 1 1.5 2 3 4 5 7 9 12 15 20 25 30];
dist_string = {'0.1' '0.15' '0.2' '0.3' '0.4' '0.5' '0.7' '1' '1.5' '2' '3' '4' '5' '7' '9' '12' '15' '20' '25' '30'};

figure(1)
boxplot(rss_meas',dist)
xlabel('Distance [m]')
ylabel('RSS [dBm]')

figure(2)
boxplot(rss_meas','position',dist,'labels',dist_string)
xlabel('Distance [m]')
ylabel('RSS [dBm]')

figure(3)
subplot(211)
boxplot(rss_meas',dist)
xlabel('Distance [m]')
ylabel('RSS [dBm]')

subplot(212)
hold on
boxplot(rss_meas','position',dist,'labels',dist_string)
xlabel('Distance [m]')
ylabel('RSS [dBm]')




ref_dist = 10; %reference distance (the x-th point) 10 => 2m

d = 0:0.1:30;
p_ref = 10.^(median(rss_meas(ref_dist,:))./10)*0.001;

iteration = 0;
n = -1;
while(iteration == 0)
    
    p_approx = p_ref * (dist(ref_dist)./d).^n;
    n = n+0.0001;
    a = p_approx(length(p_approx));
    b = 10.^(median(rss_meas(length(rss_meas),:))./10)*0.001;
    if(a <= b)
        iteration = 1;
        n
    end       
end

RSS_approx = 10*log10(p_approx./0.001);

figure(4)
hold on
boxplot(rss_meas','position',dist,'labels',dist_string)
xlabel('Distance [m]')
ylabel('RSS [dBm]')
plot(d,RSS_approx,'r')
plot(dist(ref_dist),median(rss_meas(ref_dist,:)),'xg')
plot(dist(length(dist)),median(rss_meas(length(rss_meas),:)),'og')
legend('Calculated Power','Reference Point','n optimisation point','Linear regression','Logarythmic regression')
hold off


% Linear regression / least squares
figure(5)

rss_mean = zeros(1,length(rss_meas(:,1)));
for i=1:length(rss_meas(:,1))
    rss_mean(i) = mean(rss_meas(i,:));
end

n = length(rss_mean);

alpha1 = (sum(rss_mean.*dist) - n*mean(dist)*mean(rss_mean)) / (sum(dist.^2) - n*mean(dist)^2);
alpha2 = mean(rss_mean) - alpha1 * mean(dist);

x = 0:0.1:30;
y = alpha1.*x + alpha2;

figure(5)
hold on
plot(x,y)
boxplot(rss_meas','position',dist,'labels',dist_string)
xlabel('Distance [m]')
ylabel('RSS [dBm]')
legend('Linear regression')
% Log regression / least squares

m = (sum(rss_mean.*log(dist./dist(ref_dist))) - n*mean(log(dist./dist(ref_dist)))*mean(rss_mean)) / (sum(log(dist./dist(ref_dist)).^2) - n*mean(log(dist./dist(ref_dist)))^2);
q = mean(rss_mean) - alpha1 * mean(log(dist./dist(ref_dist)));

figure(6)
y_log = m.*log(dist./dist(ref_dist)) + q;
hold on
plot(dist,y_log)
legend('Calculated Power','Reference Point','n optimisation point','Linear regression','Logarythmic regression')
hold off