function out=Qdot(qbn, wnbb);

%---------------------------------------------------------------------
% Filename : Qdot.m
% Author   : C. Eck
% Date     : 23.7.97
% Revision :
%
% Description:
% Calculate the time derivative of the quaternion representation
% of the direction cosine matrix DCM expressed by qbn.
%
% Input  : actual quaternions qbn, body to nav. rate in body frame
% Output : time derivatives of quaternions a, b, c, d
% Global : 
%---------------------------------------------------------------------

[n,m] = size(qbn);

if (n~=4)
  wnbb = [qbn(5) qbn(6) qbn(7)]';
  qbn  = [qbn(1) qbn(2) qbn(3) qbn(4)]';
end;

pnbb = [0 wnbb']';

out = 0.5*Qmul(qbn, pnbb);
