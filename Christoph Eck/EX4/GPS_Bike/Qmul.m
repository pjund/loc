function out=Qmul(q,p);

%---------------------------------------------------------------------
% Filename : Qmul.m
% Author   : C. Eck
% Date     : 23.7.97
% Revision :
%
% Description:
% Multiplication algorithm of two quaternions.
%
% Input  : Two quaternions q and p.
% Output : Product of q times p
% Global : 
%---------------------------------------------------------------------

[n,m] = size(q);

if (n~=4) % e.g Simulink Flow Diagram
  p = [q(5) q(6) q(7) q(8)];
  q = [q(1) q(2) q(3) q(4)];
end;

a = q(1);
b = q(2);
c = q(3);
d = q(4);

out = [a -b -c -d;...
       b  a -d  c;...
       c  d  a -b;...
       d -c  b  a]*p;
