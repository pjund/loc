function out=EA2Q(phi, theta, psi);

%---------------------------------------------------------------------
% Filename : EA2Q.m
% Author   : Ch. Eck
% Date     : 23.7.97
% Revision : 19.5.98 Ersatz der Titterton-Gleichungen via DCM-Matrix
%            20.5.98 Korrektur der ehemaligen Glg. mit Bose Unterlagen
%
% Description:
% Transform Euler Angles to quaternion representation.
%
% Input  : Euler angles of Cbn called EA
% Output : Quaternions of Cbn called qbn
% Global : 
%---------------------------------------------------------------------

[n,m] = size(phi);

if (n~=1)  % z.B. Ausruf aus Simulink-Blockschaltbild
  theta = phi(2);
  psi   = phi(3);
  phi   = phi(1);
end;

cp2 = cos(phi/2);
sp2 = sin(phi/2);
ct2 = cos(theta/2);
st2 = sin(theta/2);
cw2 = cos(psi/2);
sw2 = sin(psi/2);

a = cw2*ct2*cp2+sw2*st2*sp2;
b = cw2*ct2*sp2-sw2*st2*cp2;
c = cw2*st2*cp2+sw2*ct2*sp2;
d = sw2*ct2*cp2-cw2*st2*sp2;

% Umstaendlichere Moethode via DCM Matrix
% Cbn = EA2DCM(phi,theta,psi);
% qbn   = DCM2Q(Cbn);
% a = qbn(1);
% b = qbn(2);
% c = qbn(3);
% d = qbn(4);

out = [a b c d]';

