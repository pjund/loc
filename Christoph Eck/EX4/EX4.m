%% Task 3
%Settings
clear, clc, close all;

FileNameHs           = 'GPS_Bike3/GPS_Bike_ride_3_HS.log'; %GPS_Car
FileNameLs           = 'GPS_Bike3/GPS_Bike_ride_3_LS.log';

%FileNameHs           = 'GPS_Bike_ride_2_HS.log'; %GPS_Car
%FileNameLs           = 'GPS_Bike_ride_2_LS.log';
%FileNameHs           = '../EX3/GPS_Bike/20170411_162930_uVRU_NAV_HS.log';
%FileNameLs           = '../EX3/GPS_Bike/20170411_162930_uVRU_NAV_LS.log';
%FileNameHs           = '../EX2/raw/Task3/20170405_002516_uVRU_NAV_HS.log';
%FileNameLs           = '../EX2/raw/Task3/20170405_002516_uVRU_NAV_HS.log';
rateHS               = 0.005;   % 200Hz
rateLS               = 0.2;     %   5Hz
figureFlag           = 1;
SaveFigures          = 1;
FigPath              = 'Fig2';
addtit               = '';
ext                  = '.fig';
R2D                  = 180/pi;
D2R                  = pi/180;

%Read Data
DataHs = load(FileNameHs);
DataLs = load(FileNameLs);
   
% High Speed Package
vru.headerHS.FrameCnt = DataHs(:,2);
vru.headerHS.Week     = DataHs(:,5);
vru.headerHS.TOW      = DataHs(:,6);
vru.headerHS.TimeStamp= DataHs(:,7);
vru.dataHS.accx       = DataHs(:,8);
vru.dataHS.accy       = DataHs(:,9);
vru.dataHS.accz       = DataHs(:,10);
vru.dataHS.omgx       = DataHs(:,11);
vru.dataHS.omgy       = DataHs(:,12);
vru.dataHS.omgz       = DataHs(:,13);
vru.dataHS.rpyx       = DataHs(:,14);
vru.dataHS.rpyy       = DataHs(:,15);
vru.dataHS.rpyz       = DataHs(:,16);
vru.dataHS.ImuTemp    = DataHs(:,17);
vru.dataHS.vn         = DataHs(:,18);
vru.dataHS.ve         = DataHs(:,19);
vru.dataHS.vd         = DataHs(:,20);
vru.dataHS.lon        = DataHs(:,21);
vru.dataHS.lat        = DataHs(:,22);
vru.dataHS.alt        = DataHs(:,23);
vru.dataHS.magHdg     = DataHs(:,24);
vru.dataHS.odo        = DataHs(:,25);
vru.dataHS.baroAlt    = DataHs(:,26);
vru.statHS.SysStat    = DataHs(:,27);
vru.statHS.AddStat    = DataHs(:,28);
vru.statHS.EKF        = DataHs(:,29);
vru.dataHS.time       = 0:rateHS:(length(vru.dataHS.lon-1)-1)/(1/rateHS);
vru.headerHS.TimeComp = (vru.headerHS.TOW/1000) + (vru.headerHS.TimeStamp/1000000);
vru.headerHS.TimeCompDiff = diff(vru.headerHS.TimeComp);

vru.status.EKF.AlignComp      = bitget(vru.statHS.EKF,1);
vru.status.EKF.BiasMean       = bitget(vru.statHS.EKF,2);
vru.status.EKF.LowDxnamics    = bitget(vru.statHS.EKF,3);
vru.status.EKF.Error          = bitget(vru.statHS.EKF,4);
vru.status.EKF.LlhAiding      = bitget(vru.statHS.EKF,5);
vru.status.EKF.LatMeas        = bitget(vru.statHS.EKF,6);
vru.status.EKF.LonMeas        = bitget(vru.statHS.EKF,7);
vru.status.EKF.AltMeas        = bitget(vru.statHS.EKF,8);
vru.status.EKF.velNedAiding   = bitget(vru.statHS.EKF,9);
vru.status.EKF.VnMeas         = bitget(vru.statHS.EKF,10);
vru.status.EKF.VeMeas         = bitget(vru.statHS.EKF,11);
vru.status.EKF.VdMeas         = bitget(vru.statHS.EKF,12);
vru.status.EKF.velBdyAiding   = bitget(vru.statHS.EKF,13);
vru.status.EKF.VxMeas         = bitget(vru.statHS.EKF,14);
vru.status.EKF.VyMeas         = bitget(vru.statHS.EKF,15);
vru.status.EKF.VzMeas         = bitget(vru.statHS.EKF,16);
vru.status.EKF.MagAiding      = bitget(vru.statHS.EKF,17);
vru.status.EKF.MagMeas        = bitget(vru.statHS.EKF,18);
vru.status.EKF.CogAiding      = bitget(vru.statHS.EKF,19);
vru.status.EKF.CogMeas        = bitget(vru.statHS.EKF,20);
vru.status.EKF.TasAiding      = bitget(vru.statHS.EKF,21);
vru.status.EKF.TasMeas        = bitget(vru.statHS.EKF,22);
vru.status.EKF.BaroAltAiding  = bitget(vru.statHS.EKF,23);
vru.status.EKF.BaroAltMeas    = bitget(vru.statHS.EKF,24);
vru.status.EKF.BaroRateAiding = bitget(vru.statHS.EKF,25);
vru.status.EKF.BaroRateMeas   = bitget(vru.statHS.EKF,26);

vru.status.EKF.FSMMODE1   = bitget(vru.statHS.EKF,29);
vru.status.EKF.FSMMODE2   = bitget(vru.statHS.EKF,30);
vru.status.EKF.FSMMODE3   = bitget(vru.statHS.EKF,31);
vru.status.EKF.FSMMODE4   = bitget(vru.statHS.EKF,32);


% Low Speed Package
vru.headerLS.FrameCnt  = DataLs(:,2);
vru.headerLS.Week      = DataLs(:,5);
vru.headerLS.TimeStamp = DataLs(:,6);
vru.dataLS.TOW         = DataLs(:,7);
vru.dataLS.Week        = DataLs(:,8);
vru.dataLS.GpsFix      = DataLs(:,9);
vru.dataLS.GpsNavFlags = DataLs(:,10);
vru.dataLS.Lon         = DataLs(:,11);
vru.dataLS.Lat         = DataLs(:,12);
vru.dataLS.Alt         = DataLs(:,13);
vru.dataLS.Vn          = DataLs(:,14);
vru.dataLS.Ve          = DataLs(:,15);
vru.dataLS.Vd          = DataLs(:,16);
vru.dataLS.COG         = DataLs(:,17);
vru.dataLS.SV          = DataLs(:,18);
vru.dataLS.PDOP        = DataLs(:,20);
vru.dataLS.time        = 0:rateLS:(length(vru.dataLS.Lon-1)-1)/(1/rateLS);


vru.dataLS.CogScaled(1:length(vru.dataLS.COG)) = nan;
for i=1:length(vru.dataLS.COG)
    vru.dataLS.CogScaled(i) = mod(vru.dataLS.COG(i), 360);
    if(vru.dataLS.CogScaled(i) > 360/2)
        vru.dataLS.CogScaled(i) = vru.dataLS.CogScaled(i) - 360;
    else
        if(vru.dataLS.CogScaled(i) < -360/2)
            vru.dataLS.CogScaled(i) = vru.dataLS.CogScaled(i) + 360;
        else
            vru.dataLS.CogScaled(i) = vru.dataLS.CogScaled(i);
        end
    end
end

%Plot Data
if(figureFlag == 1)    
     if(SaveFigures == 1)                         
         if(exist(FigPath,'dir'))
             cd(FigPath)
         else
            mkdir(FigPath) 
            cd(FigPath)
         end
     end
     
     % Acceleration
     figure('Name', 'iuVRU Accelerometers');
     a(1) = subplot(4, 1, 1);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.accx, 'r');
     title('Acceleration [m/s?]','fontsize',12);
     ylabel('AccX [m/s^2]');
     grid;
     hold off;

     a(2) = subplot(4, 1, 2);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.accy, 'r')
     ylabel('AccY [m/s^2]');
     grid;
     hold off;

     a(3) = subplot(4, 1, 3);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.accz, 'r')     
     ylabel('AccZ [m/s^2]');
     grid;
     hold off;
     
     a(4) = subplot(4, 1, 4);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.ImuTemp, 'b')
     xlabel('Time [sec]');
     ylabel('Temperature [?C]');
     grid;
     hold off;
    
     linkaxes([a(1) a(2) a(3) a(4)],'x');  % Base Y-limits on bottom subplot
     clear a;
    
     if(SaveFigures == 1)
        saveas(gcf,['Acc',addtit,ext]);
     end
     
     % Rates
     figure('Name', 'iuVRU Gyros');
     a(1) = subplot(3, 1, 1);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.omgx, 'r');
     title('Turn Rates [deg/sec]','fontsize',12);
     ylabel('OmgX [deg/sec]');
     grid;
     hold off;

     a(2) = subplot(3, 1, 2);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.omgy, 'r')
     ylabel('OmgY [deg/sec]');
     grid;
     hold off;

     a(3) = subplot(3, 1, 3);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.omgz, 'r')
     xlabel('Time [sec]');
     ylabel('OmgZ [deg/sec]');
     grid;
     hold off;
    
     linkaxes([a(1) a(2) a(3)],'x');  % Base Y-limits on bottom subplot
     clear a;
    
     if(SaveFigures == 1)
        saveas(gcf,['Omg',addtit,ext]);
     end
     
     % Attitude
     figure('Name', 'iuVRU Attitude');
     a(1) = subplot(3, 1, 1);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.rpyx*R2D, 'r');
     title('Attitude [deg]','fontsize',12);
     ylabel('Roll [deg]');
     grid;
     hold off;

     a(2) = subplot(3, 1, 2);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.rpyy*R2D, 'r')
     ylabel('Pitch [deg]');
     grid;
     hold off;

     a(3) = subplot(3, 1, 3);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.rpyz*R2D, 'r')
     %%plot(vru.dataLS.time,vru.dataLS.CogScaled, 'b')
     plot(vru.dataHS.time,vru.dataHS.magHdg*R2D, 'm')
     
     xlabel('Time [sec]');
     ylabel('Yaw [deg]');
     grid;
     legend('INS', 'GNSS (COG)', 'MAG');
     hold off;
    
     linkaxes([a(1) a(2) a(3)],'x');  % Base Y-limits on bottom subplot
     clear a;
    
     if(SaveFigures == 1)
        saveas(gcf,['Rpy',addtit,ext]);
     end
     
     % Velocity
     figure('Name', 'iuVRU Velocity');
     a(1) = subplot(4, 1, 1);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.vn, 'r');
     plot(vru.dataLS.time,vru.dataLS.Vn, 'b');
     title('Velocity [m/s]','fontsize',12);
     ylabel('Vn [m/s]');
     grid;
     legend('INS', 'GNSS');
     hold off;

     a(2) = subplot(4, 1, 2);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.ve, 'r')
     plot(vru.dataLS.time,vru.dataLS.Ve, 'b');
     ylabel('Ve [m/s]');
     grid;
     legend('INS', 'GNSS');
     hold off;

     a(3) = subplot(4, 1, 3);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.vd, 'r')
     plot(vru.dataLS.time,vru.dataLS.Vd, 'b');
     ylabel('Vd [m/s]');
     grid;
     legend('INS', 'GNSS');
     hold off;
     
     a(4) = subplot(4, 1, 4);
     hold on;    
     plot(vru.dataHS.time,sqrt(vru.dataHS.vn.^2 + vru.dataHS.ve.^2 + vru.dataHS.vd.^2)*3.6, 'r')
     plot(vru.dataLS.time,sqrt(vru.dataLS.Vn.^2 + vru.dataLS.Ve.^2 + vru.dataLS.Vd.^2)*3.6, 'b')
     xlabel('Time [sec]');
     ylabel('Speed [km/h]');
     grid;
     legend('INS', 'GNSS');
     hold off;
    
     linkaxes([a(1) a(2) a(3) a(4)],'x');  % Base Y-limits on bottom subplot
     clear a;
    
     if(SaveFigures == 1)
        saveas(gcf,['Vel',addtit,ext]);
     end
     
    % Position
     figure('Name', 'iuVRU Position');
     a(1) = subplot(3, 1, 1);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.lon, 'r');
     plot(vru.dataLS.time,vru.dataLS.Lon, 'b');
     legend('INS', 'GNSS');
     title('Position','fontsize',12);
     ylabel('Longitude [deg]');
     grid;
     hold off;

     a(2) = subplot(3, 1, 2);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.lat, 'r')
     plot(vru.dataLS.time,vru.dataLS.Lat, 'b');
     legend('INS', 'GNSS');
     ylabel('Latitude [deg]');
     xlabel('Time [sec]');
     grid;
     hold off;

     a(3) = subplot(3, 1, 3);
     hold on;    
     plot(vru.dataHS.time,vru.dataHS.alt, 'r')
     plot(vru.dataLS.time,vru.dataLS.Alt, 'b')
     plot(vru.dataHS.time,vru.dataHS.baroAlt, 'm')
     legend('INS', 'GNSS', 'Baro');
     ylabel('Altitude');
     grid;
     hold off;
    
     if(SaveFigures == 1)
        saveas(gcf,['Pos',addtit,ext]);
     end
     
     figure('Name', 'iuVRU PositionPlot');     
     hold on;    
     plot(vru.dataHS.lon,vru.dataHS.lat, 'r');
     plot(vru.dataLS.Lon,vru.dataLS.Lat, 'b');
     legend('INS', 'GNSS');
     title('Position','fontsize',12);
     ylabel('Latitude [deg]');
     xlabel('Longitude [deg]');
     grid;     
     
     % GNSS
     figure('Name', 'iuVRU GNSS');
     a(1) = subplot(3, 1, 1);
     hold on;    
     plot(vru.dataLS.time,vru.dataLS.PDOP, 'r');
     title('GNSS Data','fontsize',12);
     ylabel('PDOP ');
     grid;
     hold off;

     a(2) = subplot(3, 1, 2);
     hold on;    
     plot(vru.dataLS.time,vru.dataLS.SV, 'r')
     ylabel('Sats');
     grid;
     hold off;

     a(3) = subplot(3, 1, 3);
     hold on;    
     plot(vru.dataLS.time,vru.dataLS.GpsFix, 'r')
     xlabel('Time [sec]');
     ylabel('Gps Fix');
     grid;
     hold off;
    
     linkaxes([a(1) a(2) a(3)],'x');  % Base Y-limits on bottom subplot
     clear a;
    
     if(SaveFigures == 1)
        saveas(gcf,['GNSS',addtit,ext]);
     end
     
     
     
     
    if(SaveFigures)
        cd('..');
    end
end

%% compute roll angle with accelerations


a_x = vru.dataHS.accx;
a_z = vru.dataHS.accz;
a_y = vru.dataHS.accy;

a_x_offset = 0.0189;
a_y_offset = 0.0123;
a_z_offset = 0.0770;


%angle offsets
phi_offset = rad2deg(atan2((-a_y-a_y_offset),(-a_z-a_z_offset)));
theta_offset = rad2deg(atan2((a_x-a_x_offset), sqrt( (a_z-a_z_offset).^2 + (a_y-a_y_offset).^2) ));



figure('name','roll and pitch angle')
subplot(211)
plot(phi_offset)
phi_offset = mean(phi_offset(1:10e3));
hold on
plot([1 10e3],[phi_offset phi_offset])
subplot(212)

plot(theta_offset)
theta_offset = mean(theta_offset(1:10e3));
hold on
plot([1 10e3], [theta_offset theta_offset])
hold off
%% Exec Task 3
% quanternion calculation
close all

with_initial_angles = 1;

if with_initial_angles
    phi = ones(1,length(vru.dataHS.omgx)) * phi_offset *D2R;
    theta = ones(1,length(vru.dataHS.omgy))* theta_offset * D2R;
else
    phi = zeros(1,length(vru.dataHS.omgx));
    theta = zeros(1,length(vru.dataHS.omgy));
end

psi = zeros(1,length(vru.dataHS.omgz));
q_dot = zeros(4,length(vru.dataHS.omgz));
q_b_n = zeros(4,length(vru.dataHS.omgz));
q_b_n(:,1) = EA2Q(phi(1),theta(1),psi(1));
os_w_phi = mean(vru.dataHS.omgx(1:601));
os_w_theta = mean(vru.dataHS.omgy(1:601));
os_w_psi = mean(vru.dataHS.omgz(1:601));

a_x_offset = 0.0189;
a_y_offset = 0.0123;
a_z_offset = 0.0770;

acc_os_x = -0.0189;
acc_os_y = -0.0123;
acc_os_z = -0.0770;

%acc_os_x = -0.0188;
%acc_os_y = -0.0124;
%acc_os_z = -0.0768;

figure('name','offset comp x')
plot(vru.dataHS.accx-acc_os_x)
ylabel('a_x [m/s^2]')
xlabel('t [s]')

figure('name','offset comp y')
plot(vru.dataHS.accy-acc_os_y)
ylabel('a_y [m/s^2]')
xlabel('t [s]')

figure('name','offset comp z')
plot(vru.dataHS.accz-acc_os_z)
ylabel('a_z [m/s^2]')
xlabel('t [s]')


acc_b = vertcat(vru.dataHS.accx'-acc_os_x,vru.dataHS.accy'-acc_os_y,vru.dataHS.accz'-acc_os_z);
acc_n = zeros(3,length(vru.dataHS.accx));
tmin = 1;
tmax = length(acc_n);



for n = 2:length(phi)
    %quaternion tranformation of current EA
    q_dot(:,n) = 0.5*[-q_b_n(2,n-1) -q_b_n(3,n-1) -q_b_n(4,n-1);
                       q_b_n(1,n-1) -q_b_n(4,n-1)  q_b_n(3,n-1);
                       q_b_n(4,n-1)  q_b_n(1,n-1) -q_b_n(2,n-1);
                      -q_b_n(3,n-1)  q_b_n(2,n-1)  q_b_n(1,n-1)]*...
                                [(vru.dataHS.omgx(n-1)-os_w_phi)*D2R;
                                 (vru.dataHS.omgy(n-1)-os_w_theta)*D2R;
                                 (vru.dataHS.omgz(n-1)-os_w_psi)*D2R];
    %propagate in the direction of the quaternion                         
    q_b_n(:,n) = q_b_n(:,n-1)+q_dot(:,n)*rateHS;
    
    %normalize quaternion
    %q_b_n(:,n) = normc(q_b_n(:,n));
    q_b_n(:,n) = q_b_n(:,n)/sqrt(sum(q_b_n(:,n).^2));
    
    %back to EA
    out = Q2EA(q_b_n(:,n));
    phi(n) = out(1)*R2D;
    theta(n) = out(2)*R2D;
    psi(n) = out(3)*R2D;
    
    
    a = q_b_n(1,n);
    b = q_b_n(2,n);
    c = q_b_n(3,n);
    d = q_b_n(4,n);
    
    %acceleration estimation in nav frame
    C_n_b = [1-2*(c^2+d^2) 2*(b*c-a*d)   2*(b*d+a*c);
             2*(b*c+a*d)   1-2*(b^2+d^2) 2*(c*d-a*b);
             2*(b*d-a*c)   2*(c*d+a*b)   1-2*(b^2+c^2)];
         
    acc_b(:,n) = acc_b(:,n)+(C_n_b)'*[0;0;9.805];
    acc_n(:,n) = C_n_b*acc_b(:,n);
         
end

figure('name','acceleration bodyframe x')
plot(acc_n(1,:))
ylabel('a_x [m/s^2]')
xlabel('t [s]')
figure('name','acceleration bodyframe y')
plot(acc_n(2,:))
ylabel('a_y [m/s^2]')
xlabel('t [s]')
figure('name','acceleration bodyframe z')
plot(acc_n(3,:))
ylabel('a_z [m/s^2]')
xlabel('t [s]')

%%


% position and velocity estimation

v = zeros(3,length(acc_n));
for n = 2:length(v)
    v(:,n) = v(:,n-1)+acc_n(:,n)*rateHS;
end

v_x = v(1,:);
v_y = v(2,:);
v_z = v(3,:);


figure('name', 'v')
subplot(311)
plot(vru.dataHS.time, v_x)
subplot(312)
plot(vru.dataHS.time, v_y)
subplot(313)
plot(vru.dataHS.time, v_z)


%% position calculation
close all
p_x = cumtrapz(v_x)*rateHS;
p_y = cumtrapz(v_y)*rateHS;
p_z = cumtrapz(v_z)*rateHS;

tmin = 1;
tmax = 217847;


fig = figure('Name','Postion Estimation')

subplot(211)
hold on 
plot(vru.dataHS.time(tmin:tmax)-vru.dataHS.time(tmin),p_x(tmin:tmax))
xlabel('time [s]')
ylabel('x [m]')
legend('estimated x by integrating','calculated v_x by uIMU')
grid on
hold off


subplot(212)
hold on 
plot(vru.dataHS.time(tmin:tmax)-vru.dataHS.time(tmin),p_y(tmin:tmax))
%plot(vru.dataHS.time(tmin:tmax)-vru.dataHS.time(tmin),-vru.dataHS.lon(tmin:tmax)+vru.dataHS.lon(1))
xlabel('time [s]')
ylabel('y [m]')
legend('estimated y by integrating','calculated v_y by uIMU')
grid on
hold off

fig = figure('Name','Table Position')
scatter3(p_x,p_y,p_z,3,vru.dataHS.time)
grid on
xlabel('x [m]')
ylabel('y [m]')
zlabel('z [m]')
c = colorbar;
c.Label.String = 't [s]';

%% Datafusion of baro and z position

%wrong aproach

close all
clc

k = 20;
p_z_baro = vru.dataHS.baroAlt';


sim('complementary_filtering_baro')


figure('name','Fusion')

subplot(311)
hold on
plot3(p_x,p_y,p_z)
plot3(p_x,p_y,zeros(1,length(p_x)))
hold off
grid on
legend('Position with imu z')
xlabel('x-Position [m]')
ylabel('y-Position [m]')
zlabel('z-Position [m]')

subplot(312)
hold on
plot3(p_x,p_y,p_z_baro-p_z_baro(1))
plot3(p_x,p_y,zeros(1,length(p_x)))
hold off
grid on
legend('Position with baro z')
xlabel('x-Position [m]')
ylabel('y-Position [m]')
zlabel('z-Position [m]')

subplot(313)
hold on
plot3(p_x,p_y,p_z_fusion.Data)
plot3(p_x,p_y,zeros(1,length(p_x)))
hold off
grid on
legend('Position Fusion')
xlabel('x-Position [m]')
ylabel('y-Position [m]')
zlabel('z-Position [m]')


%% Complementary Filtering Baro Accelerations
%right approach
clc
p_z_baro = vru.dataHS.baroAlt';
sim('complementary_filtering_fb_baro_imu')



%% Datafusion of baro and imu height with complementary filtering and feedback 

%wrong model

sim('complementary_filtering_feedback')

figure('name','Complementary feedback')
hold on
plot3(p_x,p_y,p_z_fusion_fb.Data)
plot3(p_x,p_y,zeros(1,length(p_x)))
hold off
grid on
legend('Position Fusion')
xlabel('x-Position [m]')
ylabel('y-Position [m]')
zlabel('z-Position [m]')


%% Datafusion of heading angle of yaw with complementary filtering 


%disturbed magnetometer

k_yaw = 1;
sim('complementary_filtering_yaw')

%% Yaw angle comparison
clc
psi_ds = downsample(psi,40);
magn = downsample(vru.dataHS.magHdg,40);

figure('name','Heading angles')
subplot(211)
scatter(vru.dataLS.Lon,vru.dataLS.Lat,3,magn*R2D)
xlim([8.103 8.116])
xlabel('Longitude [�]')
ylabel('Latitude [�]')
c = colorbar;
c.Label.String = 'magnetometer Heading';

subplot(212)
scatter(vru.dataLS.Lon,vru.dataLS.Lat,3,psi_ds)
xlim([8.103 8.116])
xlabel('Longitude [�]')
ylabel('Latitude [�]')
c = colorbar;
c.Label.String = 'Yaw-Angle';

%% Velocity
close all
clc
speed_imu = sqrt(v_x.^2 + v_y.^2);
speed_gps = sqrt(vru.dataLS.Ve.^2 + vru.dataLS.Vn.^2);

figure('name','speed comparison')
subplot(211)
plot(vru.dataHS.time, speed_imu)
ylabel('v [m/s]')
xlabel('t [s]')
legend('IMU Speed')


subplot(212)
plot(vru.dataLS.time,speed_gps)
ylabel('v [m/s]')
xlabel('t [s]')
legend('GPS Speed')

k_v = 3;
sim('complementary_filtering_speed')

comp_speed_ds = downsample(comp_speed.Data,40);

figure('name','Velocity')
subplot(211)
scatter(vru.dataLS.Lon,vru.dataLS.Lat,3,comp_speed_ds)
xlim([8.103 8.116])
xlabel('Longitude [�]')
ylabel('Latitude [�]')
c = colorbar;
c.Label.String = 'Complementary speed';
title('Complementary')

imu_speed_hp_ds = downsample(imu_speed_hp.Data,40);
subplot(212)
scatter(vru.dataLS.Lon,vru.dataLS.Lat,3,imu_speed_hp_ds)
c = colorbar;
c.Label.String = 'High Pass IMU'
title('High Pass IMU')
%% Position
clc
close all

figure('name','Position IMU')
subplot(211)
scatter(p_x,p_y,3,vru.dataHS.time)
xlabel('x [m]')
ylabel('y [m]')
c = colorbar;
c.Label.String = 'Time';

subplot(212)
scatter(vru.dataLS.Lon,vru.dataLS.Lat,3,vru.dataLS.time)
xlim([8.103 8.116])
xlabel('Longitude [�]')
ylabel('Latitude [�]')
c = colorbar;
c.Label.String = 'Time';



R = 6371600;
x_gps = R .* cos((vru.dataLS.Lat)*D2R) .* cos((vru.dataLS.Lon)*D2R);
y_gps = R .* cos((vru.dataLS.Lat)*D2R) .* sin((vru.dataLS.Lon)*D2R);
z_gps = R .* sin((vru.dataLS.Lat)*D2R);

figure('name','GPS_Position')
plot(y_gps,-x_gps)
