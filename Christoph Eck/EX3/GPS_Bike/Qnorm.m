function out=Qnorm(qbn);

%---------------------------------------------------------------------
% Filename : Qnorm.m
% Author   : C. Eck
% Date     : 23.7.97
% Revision :
%
% Description:
% Self consistency checks of the quaternion representation
% representing the DCM matrix Cbn expressed with qbn.
%
% Input  : quaternion representation qbn
% Output : corrected quaternion representation qbnCorr
% Global : 
%---------------------------------------------------------------------

out = qbn/sqrt(qbn(1)^2+qbn(2)^2+qbn(3)^2+qbn(4)^2);
