function out=Q2EA(qbn);

%---------------------------------------------------------------------
% Filename : Q2EA.m
% Author   : Ch. Eck
% Date     : 19.5.98
% Revision : 19.5.98 Berechnung von psi geaendert
%
% Description:
% Transform quaternion representation to Euler angles.
%
% Input  : Quaternions of Cbn called qbn
% Output : Euler angles of Cbn called EA
% Global : 
%---------------------------------------------------------------------

% alte Methoden
a = qbn(1);
b = qbn(2);
c = qbn(3);
d = qbn(4);

phi   = atan2(2*(c*d+a*b),(a^2-b^2-c^2+d^2));
theta = asin(-2*(b*d-a*c));

% alte Methode
% psi   = atan(2*(b*c+a*d)/(a^2+b^2-c^2-d^2));

% neue Methode
c11 = a^2+b^2-c^2-d^2;
c21 = 2*(b*c+a*d);
c31 = 2*(b*d-a*c);
koeffcpsi= c11/cos(asin(-c31));
koeffspsi= c21/cos(asin(-c31));
psi   = atan2(koeffspsi,koeffcpsi);

% if (psi < 0),
%   psi = psi+2*pi;
% end;

out = [phi theta psi]';


return;

% NICHT VERWENDEN!
% Umrechnung gem�ss Farrell/Barth: stimmt hier nicht mit den Reihenfolgen!!
b1 = qbn(2);
b2 = qbn(3);
b3 = qbn(4);
b4 = qbn(1);

theta = asin(-2*(b2*b4+b1*b3));
phi = atan2(2*(b2*b3-b1*b4),1-2*(b1^2+b2^2));
psi = atan2(2*(b1*b2-b3*b4),1-2*(b2^2+b3^2));




