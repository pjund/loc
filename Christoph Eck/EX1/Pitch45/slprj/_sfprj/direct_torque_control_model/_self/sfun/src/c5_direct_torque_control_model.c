/* Include files */

#include "direct_torque_control_model_sfun.h"
#include "c5_direct_torque_control_model.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "direct_torque_control_model_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c5_debug_family_names[18] = { "voltage_vector", "S1_current",
  "S2_current", "S3_current", "T_switch", "T_control", "S_current", "S", "Diff",
  "nargin", "nargout", "signals", "S1", "S2", "S3", "t_s1", "t_s2", "t_s3" };

/* Function Declarations */
static void initialize_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void initialize_params_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void enable_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void disable_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void c5_update_debugger_state_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void set_sim_state_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_st);
static void finalize_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void sf_gateway_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void mdl_start_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void c5_chartstep_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void initSimStructsc5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber);
static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, void *c5_inData);
static real_T c5_emlrt_marshallIn(SFc5_direct_torque_control_modelInstanceStruct
  *chartInstance, const mxArray *c5_b_t_s3, const char_T *c5_identifier);
static real_T c5_b_emlrt_marshallIn
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static void c5_c_emlrt_marshallIn(SFc5_direct_torque_control_modelInstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId,
  real_T c5_y[3]);
static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_d_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static int32_T c5_d_emlrt_marshallIn
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static uint8_T c5_e_emlrt_marshallIn
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_b_is_active_c5_direct_torque_control_model, const char_T *c5_identifier);
static uint8_T c5_f_emlrt_marshallIn
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_u, const emlrtMsgIdentifier *c5_parentId);
static void init_dsm_address_info(SFc5_direct_torque_control_modelInstanceStruct
  *chartInstance);
static void init_simulink_io_address
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc5_direct_torque_control_model(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c5_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c5_is_active_c5_direct_torque_control_model = 0U;
}

static void initialize_params_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c5_update_debugger_state_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  const mxArray *c5_st;
  const mxArray *c5_y = NULL;
  real_T c5_hoistedGlobal;
  const mxArray *c5_b_y = NULL;
  real_T c5_b_hoistedGlobal;
  const mxArray *c5_c_y = NULL;
  real_T c5_c_hoistedGlobal;
  const mxArray *c5_d_y = NULL;
  real_T c5_d_hoistedGlobal;
  const mxArray *c5_e_y = NULL;
  real_T c5_e_hoistedGlobal;
  const mxArray *c5_f_y = NULL;
  real_T c5_f_hoistedGlobal;
  const mxArray *c5_g_y = NULL;
  uint8_T c5_g_hoistedGlobal;
  const mxArray *c5_h_y = NULL;
  c5_st = NULL;
  c5_st = NULL;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_createcellmatrix(7, 1), false);
  c5_hoistedGlobal = *chartInstance->c5_S1;
  c5_b_y = NULL;
  sf_mex_assign(&c5_b_y, sf_mex_create("y", &c5_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c5_y, 0, c5_b_y);
  c5_b_hoistedGlobal = *chartInstance->c5_S2;
  c5_c_y = NULL;
  sf_mex_assign(&c5_c_y, sf_mex_create("y", &c5_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c5_y, 1, c5_c_y);
  c5_c_hoistedGlobal = *chartInstance->c5_S3;
  c5_d_y = NULL;
  sf_mex_assign(&c5_d_y, sf_mex_create("y", &c5_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c5_y, 2, c5_d_y);
  c5_d_hoistedGlobal = *chartInstance->c5_t_s1;
  c5_e_y = NULL;
  sf_mex_assign(&c5_e_y, sf_mex_create("y", &c5_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c5_y, 3, c5_e_y);
  c5_e_hoistedGlobal = *chartInstance->c5_t_s2;
  c5_f_y = NULL;
  sf_mex_assign(&c5_f_y, sf_mex_create("y", &c5_e_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c5_y, 4, c5_f_y);
  c5_f_hoistedGlobal = *chartInstance->c5_t_s3;
  c5_g_y = NULL;
  sf_mex_assign(&c5_g_y, sf_mex_create("y", &c5_f_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c5_y, 5, c5_g_y);
  c5_g_hoistedGlobal =
    chartInstance->c5_is_active_c5_direct_torque_control_model;
  c5_h_y = NULL;
  sf_mex_assign(&c5_h_y, sf_mex_create("y", &c5_g_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c5_y, 6, c5_h_y);
  sf_mex_assign(&c5_st, c5_y, false);
  return c5_st;
}

static void set_sim_state_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_st)
{
  const mxArray *c5_u;
  chartInstance->c5_doneDoubleBufferReInit = true;
  c5_u = sf_mex_dup(c5_st);
  *chartInstance->c5_S1 = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("S1", c5_u, 0)), "S1");
  *chartInstance->c5_S2 = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("S2", c5_u, 1)), "S2");
  *chartInstance->c5_S3 = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("S3", c5_u, 2)), "S3");
  *chartInstance->c5_t_s1 = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("t_s1", c5_u, 3)), "t_s1");
  *chartInstance->c5_t_s2 = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("t_s2", c5_u, 4)), "t_s2");
  *chartInstance->c5_t_s3 = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("t_s3", c5_u, 5)), "t_s3");
  chartInstance->c5_is_active_c5_direct_torque_control_model =
    c5_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "is_active_c5_direct_torque_control_model", c5_u, 6)),
    "is_active_c5_direct_torque_control_model");
  sf_mex_destroy(&c5_u);
  c5_update_debugger_state_c5_direct_torque_control_model(chartInstance);
  sf_mex_destroy(&c5_st);
}

static void finalize_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  int32_T c5_i0;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
  for (c5_i0 = 0; c5_i0 < 9; c5_i0++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c5_signals)[c5_i0], 0U, 1U, 0U,
                          chartInstance->c5_sfEvent, false);
  }

  chartInstance->c5_sfEvent = CALL_EVENT;
  c5_chartstep_c5_direct_torque_control_model(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_direct_torque_control_modelMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c5_S1, 1U, 1U, 0U,
                        chartInstance->c5_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c5_S2, 2U, 1U, 0U,
                        chartInstance->c5_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c5_S3, 3U, 1U, 0U,
                        chartInstance->c5_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c5_t_s1, 4U, 1U, 0U,
                        chartInstance->c5_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c5_t_s2, 5U, 1U, 0U,
                        chartInstance->c5_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c5_t_s3, 6U, 1U, 0U,
                        chartInstance->c5_sfEvent, false);
}

static void mdl_start_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c5_chartstep_c5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  int32_T c5_i1;
  uint32_T c5_debug_family_var_map[18];
  real_T c5_b_signals[9];
  real_T c5_voltage_vector;
  real_T c5_S1_current;
  real_T c5_S2_current;
  real_T c5_S3_current;
  real_T c5_T_switch;
  real_T c5_T_control;
  real_T c5_S_current[3];
  real_T c5_S[3];
  real_T c5_Diff[3];
  real_T c5_nargin = 1.0;
  real_T c5_nargout = 6.0;
  real_T c5_b_S1;
  real_T c5_b_S2;
  real_T c5_b_S3;
  real_T c5_b_t_s1;
  real_T c5_b_t_s2;
  real_T c5_b_t_s3;
  int32_T c5_i2;
  int32_T c5_i3;
  int32_T c5_i4;
  int32_T c5_i5;
  int32_T c5_i6;
  int32_T c5_i7;
  int32_T c5_i8;
  int32_T c5_i9;
  int32_T c5_i10;
  int32_T c5_i11;
  static real_T c5_dv0[3] = { 1.0, 0.0, 0.0 };

  static real_T c5_dv1[3] = { 1.0, 1.0, 0.0 };

  static real_T c5_dv2[3] = { 0.0, 1.0, 0.0 };

  static real_T c5_dv3[3] = { 0.0, 1.0, 1.0 };

  static real_T c5_dv4[3] = { 0.0, 0.0, 1.0 };

  static real_T c5_dv5[3] = { 1.0, 0.0, 1.0 };

  int32_T c5_i12;
  int32_T c5_i13;
  int32_T c5_i14;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
  for (c5_i1 = 0; c5_i1 < 9; c5_i1++) {
    c5_b_signals[c5_i1] = (*chartInstance->c5_signals)[c5_i1];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 18U, 18U, c5_debug_family_names,
    c5_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_voltage_vector, 0U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_S1_current, 1U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_S2_current, 2U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_S3_current, 3U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_T_switch, 4U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_T_control, 5U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c5_S_current, 6U, c5_c_sf_marshallOut,
    c5_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c5_S, 7U, c5_c_sf_marshallOut,
    c5_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c5_Diff, 8U, c5_c_sf_marshallOut,
    c5_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargin, 9U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargout, 10U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c5_b_signals, 11U, c5_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_S1, 12U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_S2, 13U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_S3, 14U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_t_s1, 15U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_t_s2, 16U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_t_s3, 17U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 3);
  c5_voltage_vector = c5_b_signals[0];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 4);
  c5_S1_current = c5_b_signals[1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 5);
  c5_S2_current = c5_b_signals[2];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 6);
  c5_S3_current = c5_b_signals[3];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 7);
  c5_b_t_s1 = c5_b_signals[4];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 8);
  c5_b_t_s2 = c5_b_signals[5];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 9);
  c5_b_t_s3 = c5_b_signals[6];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 10);
  c5_T_switch = c5_b_signals[7];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 11);
  c5_T_control = c5_b_signals[8];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 14);
  c5_S_current[0] = c5_S1_current;
  c5_S_current[1] = c5_S2_current;
  c5_S_current[2] = c5_S3_current;
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 15);
  for (c5_i2 = 0; c5_i2 < 3; c5_i2++) {
    c5_S[c5_i2] = 0.0;
  }

  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 17);
  switch ((int32_T)sf_integer_check(chartInstance->S, 1U, 329U, 14U,
           c5_voltage_vector)) {
   case 0:
    CV_EML_SWITCH(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 19);
    for (c5_i3 = 0; c5_i3 < 3; c5_i3++) {
      c5_S[c5_i3] = 0.0;
    }
    break;

   case 1:
    CV_EML_SWITCH(0, 1, 0, 2);
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 21);
    for (c5_i4 = 0; c5_i4 < 3; c5_i4++) {
      c5_S[c5_i4] = c5_dv0[c5_i4];
    }
    break;

   case 2:
    CV_EML_SWITCH(0, 1, 0, 3);
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 23);
    for (c5_i5 = 0; c5_i5 < 3; c5_i5++) {
      c5_S[c5_i5] = c5_dv1[c5_i5];
    }
    break;

   case 3:
    CV_EML_SWITCH(0, 1, 0, 4);
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 25);
    for (c5_i6 = 0; c5_i6 < 3; c5_i6++) {
      c5_S[c5_i6] = c5_dv2[c5_i6];
    }
    break;

   case 4:
    CV_EML_SWITCH(0, 1, 0, 5);
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 27);
    for (c5_i7 = 0; c5_i7 < 3; c5_i7++) {
      c5_S[c5_i7] = c5_dv3[c5_i7];
    }
    break;

   case 5:
    CV_EML_SWITCH(0, 1, 0, 6);
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 29);
    for (c5_i8 = 0; c5_i8 < 3; c5_i8++) {
      c5_S[c5_i8] = c5_dv4[c5_i8];
    }
    break;

   case 6:
    CV_EML_SWITCH(0, 1, 0, 7);
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 31);
    for (c5_i9 = 0; c5_i9 < 3; c5_i9++) {
      c5_S[c5_i9] = c5_dv5[c5_i9];
    }
    break;

   case 7:
    CV_EML_SWITCH(0, 1, 0, 8);
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 33);
    for (c5_i10 = 0; c5_i10 < 3; c5_i10++) {
      c5_S[c5_i10] = 1.0;
    }
    break;

   default:
    CV_EML_SWITCH(0, 1, 0, 0);
    break;
  }

  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 37);
  for (c5_i11 = 0; c5_i11 < 3; c5_i11++) {
    c5_Diff[c5_i11] = c5_S[c5_i11] - c5_S_current[c5_i11];
  }

  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 39);
  if (CV_EML_IF(0, 1, 0, CV_RELATIONAL_EVAL(4U, 0U, 0, c5_Diff[0], 1.0, -1, 0U,
        c5_Diff[0] == 1.0))) {
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 40);
    if (CV_EML_IF(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 1, c5_b_t_s1, 0.0, -1, 3U,
          c5_b_t_s1 <= 0.0))) {
      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 41);
      c5_b_t_s1 = c5_T_switch;
    } else {
      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 43);
      for (c5_i12 = 0; c5_i12 < 3; c5_i12++) {
        c5_S[c5_i12] = c5_S_current[c5_i12];
      }

      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 44);
      c5_b_t_s1 -= c5_T_control;
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 49);
  if (CV_EML_IF(0, 1, 2, CV_RELATIONAL_EVAL(4U, 0U, 2, c5_Diff[1], 1.0, -1, 0U,
        c5_Diff[1] == 1.0))) {
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 50);
    if (CV_EML_IF(0, 1, 3, CV_RELATIONAL_EVAL(4U, 0U, 3, c5_b_t_s2, 0.0, -1, 3U,
          c5_b_t_s2 <= 0.0))) {
      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 51);
      c5_b_t_s2 = c5_T_switch;
    } else {
      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 53);
      for (c5_i13 = 0; c5_i13 < 3; c5_i13++) {
        c5_S[c5_i13] = c5_S_current[c5_i13];
      }

      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 54);
      c5_b_t_s2 -= c5_T_control;
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 59);
  if (CV_EML_IF(0, 1, 4, CV_RELATIONAL_EVAL(4U, 0U, 4, c5_Diff[2], 1.0, -1, 0U,
        c5_Diff[2] == 1.0))) {
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 60);
    if (CV_EML_IF(0, 1, 5, CV_RELATIONAL_EVAL(4U, 0U, 5, c5_b_t_s3, 0.0, -1, 3U,
          c5_b_t_s3 <= 0.0))) {
      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 61);
      c5_b_t_s3 = c5_T_switch;
    } else {
      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 63);
      for (c5_i14 = 0; c5_i14 < 3; c5_i14++) {
        c5_S[c5_i14] = c5_S_current[c5_i14];
      }

      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 64);
      c5_b_t_s3 -= c5_T_control;
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 68);
  c5_b_S1 = c5_S[0];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 69);
  c5_b_S2 = c5_S[1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 70);
  c5_b_S3 = c5_S[2];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, -70);
  _SFD_SYMBOL_SCOPE_POP();
  *chartInstance->c5_S1 = c5_b_S1;
  *chartInstance->c5_S2 = c5_b_S2;
  *chartInstance->c5_S3 = c5_b_S3;
  *chartInstance->c5_t_s1 = c5_b_t_s1;
  *chartInstance->c5_t_s2 = c5_b_t_s2;
  *chartInstance->c5_t_s3 = c5_b_t_s3;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
}

static void initSimStructsc5_direct_torque_control_model
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber)
{
  (void)c5_machineNumber;
  (void)c5_chartNumber;
  (void)c5_instanceNumber;
}

static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, void *c5_inData)
{
  const mxArray *c5_mxArrayOutData;
  real_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_direct_torque_control_modelInstanceStruct *chartInstance;
  chartInstance = (SFc5_direct_torque_control_modelInstanceStruct *)
    chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_mxArrayOutData = NULL;
  c5_u = *(real_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static real_T c5_emlrt_marshallIn(SFc5_direct_torque_control_modelInstanceStruct
  *chartInstance, const mxArray *c5_b_t_s3, const char_T *c5_identifier)
{
  real_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_thisId.bParentIsCell = false;
  c5_y = c5_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_t_s3), &c5_thisId);
  sf_mex_destroy(&c5_b_t_s3);
  return c5_y;
}

static real_T c5_b_emlrt_marshallIn
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  real_T c5_y;
  real_T c5_d0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_d0, 1, 0, 0U, 0, 0U, 0);
  c5_y = c5_d0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_b_t_s3;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  real_T c5_y;
  SFc5_direct_torque_control_modelInstanceStruct *chartInstance;
  chartInstance = (SFc5_direct_torque_control_modelInstanceStruct *)
    chartInstanceVoid;
  c5_b_t_s3 = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_thisId.bParentIsCell = false;
  c5_y = c5_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_t_s3), &c5_thisId);
  sf_mex_destroy(&c5_b_t_s3);
  *(real_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData;
  int32_T c5_i15;
  const mxArray *c5_y = NULL;
  real_T c5_u[9];
  SFc5_direct_torque_control_modelInstanceStruct *chartInstance;
  chartInstance = (SFc5_direct_torque_control_modelInstanceStruct *)
    chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_mxArrayOutData = NULL;
  for (c5_i15 = 0; c5_i15 < 9; c5_i15++) {
    c5_u[c5_i15] = (*(real_T (*)[9])c5_inData)[c5_i15];
  }

  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u, 0, 0U, 1U, 0U, 1, 9), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData;
  int32_T c5_i16;
  const mxArray *c5_y = NULL;
  real_T c5_u[3];
  SFc5_direct_torque_control_modelInstanceStruct *chartInstance;
  chartInstance = (SFc5_direct_torque_control_modelInstanceStruct *)
    chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_mxArrayOutData = NULL;
  for (c5_i16 = 0; c5_i16 < 3; c5_i16++) {
    c5_u[c5_i16] = (*(real_T (*)[3])c5_inData)[c5_i16];
  }

  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u, 0, 0U, 1U, 0U, 2, 1, 3), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static void c5_c_emlrt_marshallIn(SFc5_direct_torque_control_modelInstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId,
  real_T c5_y[3])
{
  real_T c5_dv6[3];
  int32_T c5_i17;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), c5_dv6, 1, 0, 0U, 1, 0U, 2, 1, 3);
  for (c5_i17 = 0; c5_i17 < 3; c5_i17++) {
    c5_y[c5_i17] = c5_dv6[c5_i17];
  }

  sf_mex_destroy(&c5_u);
}

static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_Diff;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  real_T c5_y[3];
  int32_T c5_i18;
  SFc5_direct_torque_control_modelInstanceStruct *chartInstance;
  chartInstance = (SFc5_direct_torque_control_modelInstanceStruct *)
    chartInstanceVoid;
  c5_Diff = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_thisId.bParentIsCell = false;
  c5_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_Diff), &c5_thisId, c5_y);
  sf_mex_destroy(&c5_Diff);
  for (c5_i18 = 0; c5_i18 < 3; c5_i18++) {
    (*(real_T (*)[3])c5_outData)[c5_i18] = c5_y[c5_i18];
  }

  sf_mex_destroy(&c5_mxArrayInData);
}

const mxArray *sf_c5_direct_torque_control_model_get_eml_resolved_functions_info
  (void)
{
  const mxArray *c5_nameCaptureInfo = NULL;
  c5_nameCaptureInfo = NULL;
  sf_mex_assign(&c5_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c5_nameCaptureInfo;
}

static const mxArray *c5_d_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData;
  int32_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_direct_torque_control_modelInstanceStruct *chartInstance;
  chartInstance = (SFc5_direct_torque_control_modelInstanceStruct *)
    chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_mxArrayOutData = NULL;
  c5_u = *(int32_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static int32_T c5_d_emlrt_marshallIn
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  int32_T c5_y;
  int32_T c5_i19;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_i19, 1, 6, 0U, 0, 0U, 0);
  c5_y = c5_i19;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_b_sfEvent;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  int32_T c5_y;
  SFc5_direct_torque_control_modelInstanceStruct *chartInstance;
  chartInstance = (SFc5_direct_torque_control_modelInstanceStruct *)
    chartInstanceVoid;
  c5_b_sfEvent = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_thisId.bParentIsCell = false;
  c5_y = c5_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_sfEvent),
    &c5_thisId);
  sf_mex_destroy(&c5_b_sfEvent);
  *(int32_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static uint8_T c5_e_emlrt_marshallIn
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_b_is_active_c5_direct_torque_control_model, const char_T *c5_identifier)
{
  uint8_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_thisId.bParentIsCell = false;
  c5_y = c5_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_b_is_active_c5_direct_torque_control_model), &c5_thisId);
  sf_mex_destroy(&c5_b_is_active_c5_direct_torque_control_model);
  return c5_y;
}

static uint8_T c5_f_emlrt_marshallIn
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance, const mxArray *
   c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  uint8_T c5_y;
  uint8_T c5_u0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_u0, 1, 3, 0U, 0, 0U, 0);
  c5_y = c5_u0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void init_dsm_address_info(SFc5_direct_torque_control_modelInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address
  (SFc5_direct_torque_control_modelInstanceStruct *chartInstance)
{
  chartInstance->c5_signals = (real_T (*)[9])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c5_S1 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c5_S2 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c5_S3 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c5_t_s1 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c5_t_s2 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 5);
  chartInstance->c5_t_s3 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 6);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c5_direct_torque_control_model_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(498938052U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3114948941U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1519829218U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(4093240160U);
}

mxArray* sf_c5_direct_torque_control_model_get_post_codegen_info(void);
mxArray *sf_c5_direct_torque_control_model_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("hOYcJq9SvyMyy5xpFgddAH");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(9);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,6,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c5_direct_torque_control_model_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c5_direct_torque_control_model_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c5_direct_torque_control_model_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("pre");
  mxArray *fallbackReason = mxCreateString("hasBreakpoints");
  mxArray *hiddenFallbackType = mxCreateString("none");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c5_direct_torque_control_model_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c5_direct_torque_control_model_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c5_direct_torque_control_model(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x7'type','srcId','name','auxInfo'{{M[1],M[5],T\"S1\",},{M[1],M[14],T\"S2\",},{M[1],M[15],T\"S3\",},{M[1],M[9],T\"t_s1\",},{M[1],M[10],T\"t_s2\",},{M[1],M[11],T\"t_s3\",},{M[8],M[0],T\"is_active_c5_direct_torque_control_model\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 7, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c5_direct_torque_control_model_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc5_direct_torque_control_modelInstanceStruct *chartInstance =
      (SFc5_direct_torque_control_modelInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _direct_torque_control_modelMachineNumber_,
           5,
           1,
           1,
           0,
           7,
           0,
           0,
           0,
           0,
           0,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation
          (_direct_torque_control_modelMachineNumber_,chartInstance->chartNumber,
           chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,
             _direct_torque_control_modelMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _direct_torque_control_modelMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"signals");
          _SFD_SET_DATA_PROPS(1,2,0,1,"S1");
          _SFD_SET_DATA_PROPS(2,2,0,1,"S2");
          _SFD_SET_DATA_PROPS(3,2,0,1,"S3");
          _SFD_SET_DATA_PROPS(4,2,0,1,"t_s1");
          _SFD_SET_DATA_PROPS(5,2,0,1,"t_s2");
          _SFD_SET_DATA_PROPS(6,2,0,1,"t_s3");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,6,0,0,1,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,1129);
        _SFD_CV_INIT_EML_IF(0,1,0,698,714,-1,828);
        _SFD_CV_INIT_EML_IF(0,1,1,718,731,759,824);
        _SFD_CV_INIT_EML_IF(0,1,2,831,847,-1,961);
        _SFD_CV_INIT_EML_IF(0,1,3,851,864,892,957);
        _SFD_CV_INIT_EML_IF(0,1,4,964,980,-1,1094);
        _SFD_CV_INIT_EML_IF(0,1,5,984,997,1025,1090);

        {
          static int caseStart[] = { -1, 349, 381, 413, 445, 477, 517, 557, 597
          };

          static int caseExprEnd[] = { 8, 355, 387, 419, 451, 483, 523, 563, 603
          };

          _SFD_CV_INIT_EML_SWITCH(0,1,0,322,344,632,9,&(caseStart[0]),
            &(caseExprEnd[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,701,713,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,1,721,730,-1,3);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,2,834,846,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,3,854,863,-1,3);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,4,967,979,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,5,987,996,-1,3);

        {
          unsigned int dimVector[1];
          dimVector[0]= 9U;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c5_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _direct_torque_control_modelMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc5_direct_torque_control_modelInstanceStruct *chartInstance =
      (SFc5_direct_torque_control_modelInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c5_signals);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c5_S1);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c5_S2);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c5_S3);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c5_t_s1);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c5_t_s2);
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c5_t_s3);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "s3VAFrHDw8AcLdLfNxyaYMC";
}

static void sf_opaque_initialize_c5_direct_torque_control_model(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc5_direct_torque_control_modelInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c5_direct_torque_control_model
    ((SFc5_direct_torque_control_modelInstanceStruct*) chartInstanceVar);
  initialize_c5_direct_torque_control_model
    ((SFc5_direct_torque_control_modelInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c5_direct_torque_control_model(void
  *chartInstanceVar)
{
  enable_c5_direct_torque_control_model
    ((SFc5_direct_torque_control_modelInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c5_direct_torque_control_model(void
  *chartInstanceVar)
{
  disable_c5_direct_torque_control_model
    ((SFc5_direct_torque_control_modelInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c5_direct_torque_control_model(void
  *chartInstanceVar)
{
  sf_gateway_c5_direct_torque_control_model
    ((SFc5_direct_torque_control_modelInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c5_direct_torque_control_model
  (SimStruct* S)
{
  return get_sim_state_c5_direct_torque_control_model
    ((SFc5_direct_torque_control_modelInstanceStruct *)sf_get_chart_instance_ptr
     (S));                             /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c5_direct_torque_control_model(SimStruct* S,
  const mxArray *st)
{
  set_sim_state_c5_direct_torque_control_model
    ((SFc5_direct_torque_control_modelInstanceStruct*)sf_get_chart_instance_ptr
     (S), st);
}

static void sf_opaque_terminate_c5_direct_torque_control_model(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc5_direct_torque_control_modelInstanceStruct*)
                    chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_direct_torque_control_model_optimization_info();
    }

    finalize_c5_direct_torque_control_model
      ((SFc5_direct_torque_control_modelInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc5_direct_torque_control_model
    ((SFc5_direct_torque_control_modelInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c5_direct_torque_control_model(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c5_direct_torque_control_model
      ((SFc5_direct_torque_control_modelInstanceStruct*)
       sf_get_chart_instance_ptr(S));
  }
}

static void mdlSetWorkWidths_c5_direct_torque_control_model(SimStruct *S)
{
  /* Set overwritable ports for inplace optimization */
  ssSetStatesModifiedOnlyInUpdate(S, 1);
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_direct_torque_control_model_optimization_info
      (sim_mode_is_rtw_gen(S), sim_mode_is_modelref_sim(S), sim_mode_is_external
       (S));
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,5);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,5,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_set_chart_accesses_machine_info(S, sf_get_instance_specialization(),
      infoStruct, 5);
    sf_update_buildInfo(S, sf_get_instance_specialization(),infoStruct,5);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,5,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,5,6);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=6; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,5);
    sf_register_codegen_names_for_scoped_functions_defined_by_chart(S);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2755845780U));
  ssSetChecksum1(S,(2121985357U));
  ssSetChecksum2(S,(1601850056U));
  ssSetChecksum3(S,(1166223232U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSetStateSemanticsClassicAndSynchronous(S, true);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c5_direct_torque_control_model(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c5_direct_torque_control_model(SimStruct *S)
{
  SFc5_direct_torque_control_modelInstanceStruct *chartInstance;
  chartInstance = (SFc5_direct_torque_control_modelInstanceStruct *)utMalloc
    (sizeof(SFc5_direct_torque_control_modelInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc5_direct_torque_control_modelInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c5_direct_torque_control_model;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c5_direct_torque_control_model;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c5_direct_torque_control_model;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c5_direct_torque_control_model;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c5_direct_torque_control_model;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c5_direct_torque_control_model;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c5_direct_torque_control_model;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c5_direct_torque_control_model;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c5_direct_torque_control_model;
  chartInstance->chartInfo.mdlStart = mdlStart_c5_direct_torque_control_model;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c5_direct_torque_control_model;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
  mdl_start_c5_direct_torque_control_model(chartInstance);
}

void c5_direct_torque_control_model_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c5_direct_torque_control_model(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c5_direct_torque_control_model(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c5_direct_torque_control_model(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c5_direct_torque_control_model_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
