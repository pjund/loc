#ifndef __c5_direct_torque_control_model_h__
#define __c5_direct_torque_control_model_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc5_direct_torque_control_modelInstanceStruct
#define typedef_SFc5_direct_torque_control_modelInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c5_sfEvent;
  boolean_T c5_doneDoubleBufferReInit;
  uint8_T c5_is_active_c5_direct_torque_control_model;
  real_T (*c5_signals)[9];
  real_T *c5_S1;
  real_T *c5_S2;
  real_T *c5_S3;
  real_T *c5_t_s1;
  real_T *c5_t_s2;
  real_T *c5_t_s3;
} SFc5_direct_torque_control_modelInstanceStruct;

#endif                                 /*typedef_SFc5_direct_torque_control_modelInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c5_direct_torque_control_model_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c5_direct_torque_control_model_get_check_sum(mxArray *plhs[]);
extern void c5_direct_torque_control_model_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
