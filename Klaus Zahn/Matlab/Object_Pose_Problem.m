%test onjpose
clear;
close all;

OPTIONS.method = 'SVD';

%size of chess board square in mm
squ = 28.78;
%focal length (in pixel) from camera calibration
foc = 886;
%do we have a webcam plugin?
havePlugin = 0;

if havePlugin == 1
    camList = webcamlist;

    if isempty(camList)
       disp('no webcam available');
       disp('hit a key to exit');
       pause();
       exit();
    end
    
    cam = webcam('Logitech');

    if isempty(cam)
       disp('Logitech HD Webcam C270 webcam not available');
       disp('hit a key to exit');
       pause();
       exit();
    end

    disp('available resolutions:');
    for i = 1:length(cam.AvailableResolutions)
        disp(cam.AvailableResolutions(i));
    end

    cam.Resolution = '640x480';
    disp('chosen resolution is: ');
    disp(cam.Resolution);

    %grab an image
    img = snapshot(cam);

    figure(1);
    imshow(img);

    disp('adjust the camera position and then hit e-key to stop');

    k=[];
    set(gcf,'keypress','k=get(gcf,''currentchar'');');

    while 1
      img = snapshot(cam);
      imshow(img);
      title('original image');

      if ~isempty(k)
        if strcmp(k,'e') 
            imwrite(img, 'grabImg.png');
            clear('cam');
            break; 
        end;
        if strcmp(k,'p') 
            pause; k=[]; 
        end;
      end
    end
else
    %adjust the image file name 
    img = imread('grabImg.png');
    
    figure(1);
    imshow(img);
    title('original image');
end
%extract the corners
corners = findCorners(img,0.01,1);
chessboards = chessboardsFromCorners(corners);

figure(1); hold on;
plotChessboards(chessboards,corners);
title('image with chessboard overlay');

%shortcut
edges = chessboards{1};

%chess board size depends on image
CB_Size = size(edges);
len = prod(CB_Size);
%size of image
Rows = size(img, 1);
Cols = size(img, 2);

%get the image points
PoiImg = corners.p(edges(:),:);

%is the chess board complete?
if length(PoiImg) ~= CB_Size(1)*CB_Size(2)
    sprintf('chess board size %d x %d does not fit to length of point vector %d',  CB_Size(1), CB_Size(2), length(PoiImg)) 
else
    %construct real chess board in millimeters (put center in the middle of the
    %board
    %p3D = [];%list of 3D object points (as columns)[];
    %q2D = [];%list of 2D image points (as columns, i.e. all x-values as first row, all y-value as second row)
    %solve pose estimation problem
    %[R, t, it, obj_err, img_err] = objpose(p3D, q2D, OPTIONS);
    %give out rotation matrix R and translation vector t
    
end

