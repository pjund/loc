clear 'all';
close 'all';

%3D example
a1 = [0 0 1]';
b1 = [2 0.5 1]';

Vec = [a1, b1];

PlotVectorList(Vec, 2);

Vec = [a1, b1, a1+b1, a1-b1, 0.5*b1];

PlotVectorList(Vec, 3);

alp = pi*56/180;
Rx = [1 0 0; 0 cos(alp) sin(alp); 0 -sin(alp) cos(alp)];
bet = pi*67/180;
Ry = [cos(bet) 0 sin(bet); 0 1 0; -sin(bet) 0 cos(bet)];
gam = pi*15/180;
Rz = [cos(gam) sin(gam) 0; -sin(gam) cos(gam) 0; 0 0 1];

R = Rz*Ry*Rx;

Vec = [a1, b1, Rx*a1, Rx*b1];

PlotVectorList(Vec, 4);