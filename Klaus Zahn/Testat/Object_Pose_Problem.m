%test onjpose
clear all;
close all;
clc
OPTIONS.method = 'SVD';

addpath('objpose/');
addpath('libcbdetect/');

%size of chess board square in mm
squ = 28.78;
%focal length (in pixel) from camera calibration
foc = 1.4236e+03;
%do we have a webcam plugin?
havePlugin = 1;

if havePlugin == 1
    camList = webcamlist;

    if isempty(camList)
       disp('no webcam available');
       disp('hit a key to exit');
       pause();
       exit();
    end

    cam = [];
    cam = webcam('USB Camera');

    if isempty(cam)
       disp('Logitech HD Webcam C270 webcam not available');
       disp('hit a key to exit');
       pause();
       exit();
    end

    disp('available resolutions:');
    for i = 1:length(cam.AvailableResolutions)
        disp(cam.AvailableResolutions(i));
    end

    cam.Resolution = '1280x960';
    disp('chosen resolution is: ');
    disp(cam.Resolution);

    %grab an image
    img = snapshot(cam);

    figure(1);
    imshow(img);

    disp('adjust the camera position and then hit e-key to stop');

    k=[];
    set(gcf,'keypress','k=get(gcf,''currentchar'');');

    while 1
      img = snapshot(cam);
      imshow(img);
      title('original image');

      if ~isempty(k)
        if strcmp(k,'e'); 
            imwrite(img, 'grabImg.png');
            clear('cam');
            break; 
        end;
        if strcmp(k,'p'); 
            pause; k=[]; 
        end;
      end
    end
else
    
    %adjust the image file name 
    img = imread('../Matlab/tr.png');
    %img = imread('poseImg.png');
    figure(1);
    imshow(img);
    title('original image');
    
end
%extract the corners
corners = findCorners(img,0.01,1);
chessboards = chessboardsFromCorners(corners);

%shortcut
edges = chessboards{1};

%get the image points
PoiImg = corners.p(edges(:),:);

figure(1); hold on;
plotChessboards(chessboards,corners);
title('image with chessboard overlay');
plot(PoiImg(9,1),PoiImg(9,2),'x');
hold off


%chess board size depends on image
CB_Size = size(edges);
len = prod(CB_Size);
%size of image
Rows = size(img, 1);
Cols = size(img, 2);

%is the chess board complete?
if length(PoiImg) ~= CB_Size(1)*CB_Size(2)
    sprintf('chess board size %d x %d does not fit to length of point vector %d',  CB_Size(1), CB_Size(2), length(PoiImg)) 
else
    
    %initialize empty vector
    chess_board = zeros(3, (CB_Size(2)) * (CB_Size(1)));
    
    %generate 2d chess board coordinates
    count = 0;
    for y=0:CB_Size(2)-1
        for x=0:CB_Size(1)-1
            count = count + 1;
            chess_board(1, count ) = (x * squ-((CB_Size(1)-1)/2)*squ);
            chess_board(2, count ) = (y * squ-((CB_Size(2)-1)/2)*squ);
        end
    end
   
   %plot generated chess board
    figure(2)
    hold on
    for i=1:length(chess_board)
        plot3(chess_board(1,i),chess_board(2,i),0,'x')
    end
    title('Raw 3D Chess Board')
    hold off
    
    figure(8)
    plot(chess_board(1,:),chess_board(2,:),'-')
    hold on
    plot(chess_board(1,1),chess_board(2,1),'x')
    plot(chess_board(1,10),chess_board(2,10),'x')
    hold off
    
    p3D = chess_board;
    
    q2D(1,:) = (PoiImg(:,1)-1280/2)/foc;
    q2D(2,:) = (PoiImg(:,2)-960/2)/foc;
    
    %q2D = corners.p';
    %q2D(1,:) = (q2D(1,:) - 1280/2);
    %q2D(2,:) = (q2D(2,:) - 960/2);
    figure(4)
    plot(PoiImg(:,1),PoiImg(:,2))
    hold on
    plot(PoiImg(1,1),PoiImg(1,2),'x')
    plot(PoiImg(10,1),PoiImg(10,2),'x')
    hold off
    
    figure(3)
    plot(q2D(1,:),q2D(2,:),'xr')
    title('Raw 2D Photo')
    
    hold on
    plot(q2D(1,1),q2D(2,1),'or')
    plot(q2D(1,10),q2D(2,10),'ob')
    
    %construct real chess board in millimeters (put center in the middle of the
    %board
    %p3D = [];%list of 3D object points (as columns)[];
    %q2D = [];%list of 2D image points (as columns, i.e. all x-values as first row, all y-value as second row)
    %solve pose estimation problem
    [R, t, it, obj_err, img_err] = objpose(p3D, q2D, OPTIONS);
    %give out rotation matrix R and translation vector t
    
    
    camera_pos = -R'*t;
    
    if CB_Size(1)>CB_Size(2) %% x greater than y, usual case
        camera_pos(1) = -camera_pos(1);
        camera_pos(2) = -camera_pos(2);

        figure(5)
        hold on
        for i=1:length(chess_board)
            plot3(chess_board(1,i),chess_board(2,i),0,'x')
            xlabel('x')
            ylabel('y')
            zlabel('z')
        end

        plot3(camera_pos(1),camera_pos(2),camera_pos(3),'o')
        hold on
        for i=1:length(chess_board)
          plot3([camera_pos(1),chess_board(1,i)],[camera_pos(2),chess_board(2,i)],[camera_pos(3),chess_board(3,i)])
        end
        hold off
    else
        camera_pos(1) = -camera_pos(1);
        camera_pos(3) = -camera_pos(3);
        figure(5)
        hold on
        for i=1:length(chess_board)
            plot3(chess_board(2,i),chess_board(1,i),0,'x')
            xlabel('x')
            ylabel('y')
            zlabel('z')
        end
        
        plot3(camera_pos(2),camera_pos(1),camera_pos(3),'o')
        hold on
        for i=1:length(chess_board)
            plot3([camera_pos(2),chess_board(2,i)],[camera_pos(1),chess_board(1,i)],[camera_pos(3),chess_board(3,i)])
        end
        hold off
    end
end

