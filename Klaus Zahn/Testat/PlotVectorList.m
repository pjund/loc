function [ Ret ] = PlotVectorList( VectorList, FigureIndex, varargin)
%plots the list of 2d or 3d vector in a new figure of index FigureIndex;
%if the figure exists it is cleared unless varargin is set to 0

DoClear = 1;%clear figure by default
if ~isempty(varargin)
    DoClear = varargin{1};
end

ArrowLen = 0.1;%default arrow length 10% of 
LineWidth = 1.5;%default line width 1.5 pt

%list of available colors 
Col = [[1 0 0]; [0 1 0]; [0 0 1]; [1 1 0]; [1 0 1]; [0 1 1]; [1 0.5 0]; [1 0 0.5]; [0.5 1 0]; [0 1 0.5]; [1 1 0.5]; [1 0.5 1]; [0.5 1 1]];
MaxC = length(Col);

[m,n] = size(VectorList);

%we assume column vectors
if m == 2
    Dim = 2;
elseif m == 3
    Dim = 3;
elseif n == 2
    Dim = 2;
    VectorList = VectorList';
elseif n ==3
    Dim = 3;
    VectorList = VectorList';
else
    Ret = 1;
    sprintf('the list does not contain vectors of dimension 2 or 3')
    return; 
end

figure(FigureIndex);
if DoClear == 1
    clf;
end
hold on;

if Dim == 2
    ColInd = 1;
    for i = 1:size(VectorList,2)
        V1 = VectorList(:,i);
        V1n = max(1,norm(V1));
        V2 = [VectorList(2,i), -VectorList(1,i)]';
        V2 = V2/max(1,norm(V2));
        HList = [zeros(2,1) , V1, V1-ArrowLen*V1+V1n*ArrowLen/2*V2, V1, V1-ArrowLen*V1-V1n*ArrowLen/2*V2];
        plot(HList(1,:),HList(2,:),'b-', 'Color',Col(ColInd,:),'LineWidth', LineWidth);
        if ColInd == MaxC
            ColInd = 1;
        else 
            ColInd = ColInd+1;
        end
    end
    xlabel('x-axis');
    ylabel('y-axis');
    axis('equal');
else
    ColInd = 1;
    for i = 1:size(VectorList,2)
        V1 = VectorList(:,i);
        V1n = max(1,norm(V1));
        V2 = [VectorList(2,i), -VectorList(1,i), 0]';
        if norm(V2) == 0
            V2 = [0, -VectorList(3,i), VectorList(2,i)]';
        end
        V2 = V2/max(1,norm(V2));
        HList = [zeros(3,1) , V1, V1-ArrowLen*V1+V1n*ArrowLen/2*V2, V1, V1-ArrowLen*V1-V1n*ArrowLen/2*V2];
        plot3(HList(1,:),HList(2,:),HList(3,:),'b-', 'Color',Col(ColInd,:),'LineWidth', LineWidth);
        if ColInd == MaxC
            ColInd = 1;
        else 
            ColInd = ColInd+1;
        end
    end
    xlabel('x-axis');
    ylabel('y-axis');
    zlabel('z-axis');
    axis('equal');
    view([37.5 30]);
    grid on;
end

Ret = 0;
