clc
clear
close all

%setup
OPTIONS.method = 'SVD';

%focal length (in pixel) from camera calibration
foc = 1.4236e+03;

%origin in mm

%p3D= [0 1640 2650 3700 4310 6220 7915; 0 -1430 -1030 -1440 -1490 -670 -1490; 0 0 0 0 0 0 0];
p3D= [0 1640 2650 3700 4310 6220 7915; 0 1430 1030 1440 1490 670 1490; 0 0 -457 0 -613 0 631];

figure(1)
plot3(p3D(1,:),p3D(2,:),p3D(3,:),'o');
xlabel('x');
ylabel('y');
zlabel('z');

%left center
lcx = [93 323 457 571 665 851 1043];
lcy = [83 279 199 283 289 195 293];

lc(1,:) = lcx;
lc(2,:) = lcy;

figure(2)
imshow('lc.png')
hold on
plot(lcx,lcy,'ro')

%center center
ccx = [109 317 443 579 655 903 1141];
ccy = [323 509 425 512 512 407 515];

cc(1,:) = ccx;
cc(2,:) = ccy;

figure(3)
imshow('cc.png')
hold on
plot(ccx,ccy,'ro')

%right center
rcx = [113 293 389 525 579 837 1063];
rcy = [211 379 295 375 381 267 377];

rc(1,:) = rcx;
rc(2,:) = rcy;

figure(4)
imshow('rc.png')
hold on
plot(rcx,rcy,'ro')

%right top
rtx = [271 429 511 633 679 907 1111];
rty = [185 335 267 337 343 245 351];

rt(1,:) = rtx;
rt(2,:) = rty;

figure(5)
imshow('rt.png')
hold on
plot(rtx,rty,'ro')

%center top
ctx = [205 407 525 651 727 941 1159];
cty = [207 371 299 369 377 269 367];

ct(1,:) = ctx;
ct(2,:) = cty;

figure(6)
imshow('ct.png')
hold on
plot(ctx,cty,'ro')
hold off
%

PoiImg = ct;
q2D(1,:) = (PoiImg(1,:)-1280/2)/foc;
q2D(2,:) = (PoiImg(2,:)-960/2)/foc;

%construct real chess board in millimeters (put center in the middle of the
%board
%p3D = [];%list of 3D object points (as columns)[];
%q2D = [];%list of 2D image points (as columns, i.e. all x-values as first row, all y-value as second row)
%solve pose estimation problem
[R, t, it, obj_err, img_err] = objpose(p3D, q2D, OPTIONS);
%give out rotation matrix R and translation vector t

camera_pos = -R'*t;

figure(7)
hold on
for i=1:length(p3D)
    plot3(p3D(1,i),p3D(2,i),0,'x')
    xlabel('x')
    ylabel('y')
    zlabel('z')
end

plot3(camera_pos(1),camera_pos(2),camera_pos(3),'o')
hold on
for i=1:length(q2D)
  plot3([camera_pos(1),p3D(1,i)],[camera_pos(2),p3D(2,i)],[camera_pos(3),p3D(3,i)])
end
hold off
grid on
axis equal vis3d

figure(8)
subplot(221)
plot(camera_pos(1),camera_pos(2),'o')
hold on
for i=1:length(q2D)
  plot([camera_pos(1),p3D(1,i)],[camera_pos(2),p3D(2,i)])
end
hold off
grid on
axis equal
xlabel('x');
ylabel('y');
subplot(223)
plot(camera_pos(3),camera_pos(2),'o')
hold on
for i=1:length(q2D)
  plot([camera_pos(3),p3D(3,i)],[camera_pos(2),p3D(2,i)])
end
hold off
grid on
axis equal
xlabel('z');
ylabel('y');
subplot(222)
plot(camera_pos(1),camera_pos(3),'o')
hold on
for i=1:length(q2D)
  plot([camera_pos(1),p3D(1,i)],[camera_pos(3),p3D(3,i)])
end
hold off
grid on
axis equal
xlabel('x');
ylabel('z');
