%test onjpose
clear all;
close all;

%size of chess board square in mm
squ = 28.78;
%distance in millimeters
dist = 300;
%do we have a webcam plugin?
havePlugin = 1;

if havePlugin == 1
    camList = webcamlist;

    if isempty(camList)
       disp('no webcam available');
       disp('hit a key to exit');
       pause();
       exit();
    end

    cam = [];
    cam = webcam('USB-Kamera');

    if isempty(cam)
       disp('Logitech HD Webcam C270 webcam not available');
       disp('hit a key to exit');
       pause();
       exit();
    end

    disp('available resolutions:');
    for i = 1:length(cam.AvailableResolutions)
        disp(cam.AvailableResolutions(i));
    end

    cam.Resolution = '1280x960';
    disp('chosen resolution is: ');
    disp(cam.Resolution);

    %grab an image
    img = snapshot(cam);

    figure(1);
    imshow(img);

    
    
    disp('adjust the camera position and then hit e-key to stop');

    k=[];
    set(gcf,'keypress','k=get(gcf,''currentchar'');');

    while 1
      img = snapshot(cam);
      imshow(img);
      title('original image');

      if ~isempty(k)
        if strcmp(k,'e'); 
            imwrite(img, 'grabImg.png');
            clear('cam');
            break; 
        end;
        if strcmp(k,'p'); 
            pause; k=[]; 
        end;
      end
    end
else
    %adjust the image file name 
    img = imread('calibImg.png');
    
    figure(1);
    imshow(img);
    title('original image');
end



%extract the corners
corners = findCorners(img,0.01,1);
chessboards = chessboardsFromCorners(corners);

figure(1); hold on;
plotChessboards(chessboards,corners);
title('image with chessboard overlay');

figure(2);hold on;
edges = chessboards{1};
xc = corners.p(edges(:),1);
yc = corners.p(edges(:),2);
plot(xc, yc, 'bo-');
%plot first one in red
plot(xc(1), yc(1), 'rx');
plot(xc(41), yc(41), 'bx')
title('chessboard edges only');


real_length = 130;
camera_length = sqrt( (xc(41)-xc(1))^2 + (yc(41)-yc(1))^2 )

f = camera_length*300/real_length

real_length = camera_length * 300 / f
